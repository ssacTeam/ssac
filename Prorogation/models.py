from django.db import models
from django.urls import reverse_lazy
import datetime

from DecDet.models import DecDetModel

class ProrogationManager(models.Manager):


    def getDureeMaxProrogation(self, object):
        duree = self. getTotalDurees(object)
        # if isinstance(object, DecDetModel):
        #     decdet = object
        #     date_debut = decdet.date_autors_at
        # elif isinstance(object, ProrogationModel):
        #     prorogation = object
        #     date_debut = prorogation.decdet.date_autors_at
        # else:
        #     return 0
        # dureeRestante = (date_debut + datetime.timedelta(days=30)*duree) - datetime.datetime.now().date()
        # return dureeRestante.days/30
        return object.duree_contrat - duree

    def getTotalDurees(self, object):
        if isinstance(object, DecDetModel):
            decdet = object
            return decdet.duree_decdet
        elif isinstance(object, ProrogationModel):
            prorogation = object
            duree_at = prorogation.decdet.duree_decdet
            durees_pr = 0
            while prorogation is not None:
                durees_pr += prorogation.duree_decdet
                prorogation = prorogation.prorogation_mere
            return duree_at + durees_pr
        else:
            return 0

    def getCreateAPIUrl(self):
        return reverse_lazy('prorogation:api-prorogation:create')

    def create_prorogation(self, prorogation_args, *args, **kwargs):
        try:
            decdet = DecDetModel.objects.get(num_decdet = prorogation_args['decdet'])
            try:
                prorogation_mere = ProrogationModel.objects.get(num_decdet = prorogation_args['prorogation_mere'])
            except ProrogationModel.DoesNotExist:
                prorogation_mere = None
            prorogation = self.model(
                decdet          = decdet,
                prorogation_mere= prorogation_mere,
                code            = prorogation_args['code'],
                num_decdet      = prorogation_args['num_decdet'],
                importateur     = prorogation_args['importateur'],
                declarant       = prorogation_args['declarant'],
                val_marchandise = prorogation_args['val_marchandise'],
                taux_suspension = prorogation_args['taux_suspension'],
                montant_dit     = prorogation_args['montant_dit'],
                numero_caution  = prorogation_args['numero_caution'],
                date_caution    = prorogation_args['date_caution'],
                num_contrat     = prorogation_args['num_contrat'],
                objet_contrat   = prorogation_args['objet_contrat'],
                lieu_execution  = prorogation_args['lieu_execution'],
                duree_contrat   = prorogation_args['duree_contrat'],
                num_autors_pr   = prorogation_args['num_autors_pr'],
                date_autors_pr  = prorogation_args['date_autors_pr'],
                duree_decdet    = prorogation_args['duree_decdet']
            )
            prorogation.save()

        except Exception as e:
            print(e)
            prorogation = None
        return prorogation



    def getApiListUrl(self):
        return reverse_lazy('prorogation:api-prorogation:list')





class ProrogationModel(models.Model):
    """docstring for ProrogationModel."""

    class Meta:
        verbose_name_plural = 'Prorogations'

    decdet              = models.ForeignKey(DecDetModel, default=1, on_delete=models.CASCADE, related_name='prorogation_set')
    prorogation_mere    = models.ForeignKey('self', on_delete=models.CASCADE, related_name='prorogation_filles', blank=True, null=True)

    code            = models.CharField(max_length = 4, default='')
    num_decdet      = models.CharField(max_length = 255, default='', unique = True)
    importateur     = models.CharField(max_length = 255, default='')
    declarant       = models.CharField(max_length = 255, default='')
    val_marchandise = models.PositiveIntegerField(default=0)
    taux_suspension = models.PositiveIntegerField(default=0)
    montant_dit     = models.PositiveIntegerField(default=0)


    numero_caution  = models.CharField(max_length = 255, default='')
    date_caution    = models.DateField(default=datetime.date.today)

    num_contrat     = models.CharField(max_length = 255, default='')
    objet_contrat   = models.CharField(max_length = 255, default='')
    lieu_execution  = models.CharField(max_length = 255, default='')
    duree_contrat   = models.PositiveIntegerField(default=0)

    num_autors_pr   = models.CharField(max_length = 255, default='')
    date_autors_pr  = models.DateField(default=datetime.date.today)
    duree_decdet    = models.PositiveIntegerField(default=0)

    timestamp       = models.DateTimeField(auto_now_add=True, blank=True)# date of creation


    objects = ProrogationManager()

    def __str__(self):
        return "Prorogation n°"+str(self.num_decdet)

    def getDetailUrl(self):
        return reverse_lazy('prorogation:detail', kwargs = {'pk':self.pk})

    def getUpdateUrl(self):
        return reverse_lazy('prorogation:update', kwargs = {'pk':self.pk})

    def getProrogationListUrl(self, *args, **kwargs):
        return reverse_lazy('prorogation:list', kwargs={'pk' : self.pk})

    def getDureeEcoulee(self):
        return ProrogationModel.objects.getTotalDurees(self) - self.duree_decdet

    def getArticleListApiUrl(self):
        return reverse_lazy('article:api-article:prorogation', kwargs = {'prorogation_pk' : self.pk})

    def getUpdateUrl(self):
        return reverse_lazy('prorogation:update', kwargs = {'pk' : self.pk})

    def get_absolute_url(self):
        return self.getDetailUrl()

    def getTransfertsListUrl(self, *args, **kwargs):
        return reverse_lazy('transfert:decdet-list', kwargs={'pk' : self.pk})

    def getAppurementApiCreateUrl(self, *args, **kwargs):
        return reverse_lazy('article:api-article:appurer')

    def getDeltaAppurement(self):
        prorogation = self
        delta_appurement = (prorogation.date_autors_pr + datetime.timedelta(days=30)*prorogation.duree_decdet) - datetime.datetime.now().date()
        return int(delta_appurement.days / 30)

    def getDureeTransfertMax(self):
        prorogation = self
        delta_appurement = (prorogation.date_autors_pr + datetime.timedelta(days=30)*prorogation.duree_decdet) - datetime.datetime.now().date()
        return int(delta_appurement.days / 30)

    def getDecdetStatus(self):
        status_appurement = ''
        nbPending = nbRetard = 0
        for article in self.article_set.all():
            article_status = article.getStatusAppurement()
            if article_status == 'retard':
                nbRetard+=1
            elif article_status == 'pending':
                nbPending+=1
        if nbRetard != 0:
            status_appurement = 'retard'
        elif nbPending !=0:
            status_appurement = 'pending'
        else:
            status_appurement = 'appure'
        return status_appurement
