from django.apps import AppConfig


class ProrogationConfig(AppConfig):
    name = 'Prorogation'
