from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.views import generic

from .forms import ProrogationCreateForm, ProrogationUpdateForm
from .models import ProrogationModel
from DecDet.models import DecDetModel
from Prorogation.models import ProrogationModel
from Transfert.models import TransfertModel
from .api.pagination import PAGE_SIZE


class ProrogationDecdetListView(generic.ListView, LoginRequiredMixin):
    model = ProrogationModel
    template_name = "prorogation_list.html"

    def get_queryset(self, *args, **kwargs):
        queryset = None
        pk = self.kwargs.get('pk')
        try:
            at = DecDetModel.objects.get(pk = pk)
            queryset = at.prorogation_set.all()
        except DecDetModel.DoesNotExist:
            try:
                prorogation = ProrogationModel.objects.get(pk = pk)
                queryset = prorogation.decdet.prorogation_set.all()
            except ProrogationModel.DoesNotExist:
                pass
        return queryset.order_by('-timestamp')

    def get_context_data(self, *args, **kwargs):
        pk = self.kwargs.get('pk')
        at = None
        try:
            at = DecDetModel.objects.get(pk = pk)
        except DecDetModel.DoesNotExist:
            try:
                prorogation = ProrogationModel.objects.get(pk = pk)
                at = prorogation.decdet
            except ProrogationModel.DoesNotExist:
                pass
        context = super(ProrogationDecdetListView, self).get_context_data()
        context['decdet']     = at
        context['decdet_api_list_url']    = at.getProrogationApiListUrl()
        context['PAGE_SIZE']              = PAGE_SIZE
        return context


class ProrogationDetailView(generic.DetailView, LoginRequiredMixin):
    model = ProrogationModel
    template_name = "prorogation_detail.html"

    def get_object(self, *args, **kwargs):
        prorogation_pk = self.kwargs.get('pk')
        prorogation = get_object_or_404(ProrogationModel, pk = prorogation_pk)
        return prorogation

    def get_context_data(self, *args, **kwargs):
        prorogation_pk = self.kwargs.get('pk')
        prorogation = self.get_object()
        context = super(ProrogationDetailView, self).get_context_data()
        context['duree_restante_prorogation']  = ProrogationModel.objects.getDureeMaxProrogation(prorogation)
        context['duree_restante_transfert']  = prorogation.getDureeTransfertMax()
        context['date_creation']        = prorogation.timestamp.strftime('%A %d %B %Y')
        context['temps_creation']       = prorogation.timestamp.strftime('%H:%M:%S')
        context['decdet_api_detail_url']= DecDetModel.objects.getApiDetailUrl()
        context['decdet_articles_url']  = prorogation.getArticleListApiUrl()
        context['transfert_article_api_url']  = TransfertModel.objects.getCreateAPIUrl()
        context['prorogation_create_api_url']    = ProrogationModel.objects.getCreateAPIUrl()

        return context


class ProrogationUpdateView(generic.UpdateView, LoginRequiredMixin):
    form_class = ProrogationUpdateForm
    template_name = "prorogation_update.html"

    def get_object(self, *args, **kwargs):
        prorogation_pk = self.kwargs.get('pk')
        prorogation = get_object_or_404(ProrogationModel, pk = prorogation_pk)
        return prorogation
