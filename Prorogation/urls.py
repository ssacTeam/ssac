from django.urls import path, include

from .views import ProrogationDecdetListView, ProrogationDetailView, ProrogationUpdateView

app_name='Prorogation'

urlpatterns = [
    path('list/<int:pk>',  ProrogationDecdetListView.as_view(), name='list'),
    path('<int:pk>',  ProrogationDetailView.as_view(), name='detail'),
    path('update/<int:pk>',  ProrogationUpdateView.as_view(), name='update'),
    path('api/', include('Prorogation.api.urls', namespace='api-prorogation')),
]
