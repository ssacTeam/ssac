from django import forms
from ssac.forms import DateInput

from .models import ProrogationModel

class ProrogationCreateForm(forms.ModelForm):
    class Meta:
        model = ProrogationModel
        fields = '__all__'




class ProrogationUpdateForm(forms.ModelForm):
    class Meta:
        model = ProrogationModel
        fields = '__all__'
        exclude = ('decdet', 'prorogation_mere',)
        labels = {
        'num_decdet' : 'N° Déclaration',
        'declarant' : 'Déclarant',
        'val_marchandise' : 'Valeur Marchandise',
        'taux_suspension' : 'Taux de suspension',
        'montant_dit' : 'Montant D/T',
        'numero_caution' : 'N° Caution',
        'num_contrat' : 'N° Contrat',
        'lieu_execution' : 'Lieu d\'exécution',
        'duree_contrat' : 'Durée du contrat',
        'num_autors_pr' : 'N° Autorisation de la prorogation',
        'date_autors_pr' : 'Date Autorisation de la prorogation',
        'duree_decdet' : 'Durée accordée',
        }
        widgets = {
        'date_caution' : DateInput(format='%Y-%m-%d'),
        'date_autors_pr' : DateInput(format='%Y-%m-%d')
        }
