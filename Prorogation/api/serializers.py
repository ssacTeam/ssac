from rest_framework import serializers

from ..models import ProrogationModel

class ProrogationListSerializer(serializers.ModelSerializer):
    decdet_detail_url        = serializers.SerializerMethodField()
    status_appurement        = serializers.SerializerMethodField()
    duree_ecoulee            = serializers.SerializerMethodField()
    nb_articles              = serializers.SerializerMethodField()
    decdet_num_decdet        = serializers.SerializerMethodField()
    prorogation_mere_num_decdet   = serializers.SerializerMethodField()
    prorogation_mere_Bool  = serializers.SerializerMethodField()



    class Meta:
        model = ProrogationModel
        fields = [
            'pk',
            'code',
            'decdet',
            'decdet_num_decdet',
            'prorogation_mere',
            'num_decdet',
            'importateur',
            'declarant',
            'val_marchandise',
            'taux_suspension',
            'montant_dit',
            'numero_caution',
            'date_caution',
            'num_contrat',
            'objet_contrat',
            'lieu_execution',
            'duree_contrat',
            'num_autors_pr',
            'date_autors_pr',
            'duree_decdet',

            'duree_ecoulee',
            'nb_articles',
            'prorogation_mere_num_decdet',
            'prorogation_mere_Bool',

            'status_appurement',
            'decdet_detail_url'
        ]

    def get_decdet_detail_url(self, object):
        return object.getDetailUrl()

    def get_status_appurement(self, object):
        return object.getDecdetStatus()

    def get_nb_articles(self, object):
        return len(object.article_set.all())

    def get_duree_ecoulee(self, object):
        return object.getDureeEcoulee()

    def get_decdet_num_decdet(self, object):
        return object.decdet.num_decdet

    def get_prorogation_mere_num_decdet(self, object):
        if self.get_prorogation_mere_Bool(object):
            return object.prorogation_mere.num_decdet
        return None

    def get_prorogation_mere_Bool(self, object):
        if object.prorogation_mere:
            return True
        return False
