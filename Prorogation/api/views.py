from django.db.models import Q
from rest_framework import generics, permissions
from rest_framework.views import APIView
from rest_framework.response import Response

from Article.models import ArticleModel

from ..models import ProrogationModel
from .serializers import ProrogationListSerializer
from .pagination import ProrogationListPagination

from CSV.CSV import prorogationQuerysetToCSV

class ProrogationDecdetAPIView(APIView):
    def get(self, request, format = None):
        prorogation_args = {}
        prorogation_args['decdet'] = self.request.GET.get('decdet', None)
        prorogation_args['prorogation_mere'] = self.request.GET.get('prorogation_mere', None)
        prorogation_args['code'] = self.request.GET.get('code', None)
        prorogation_args['num_decdet'] = self.request.GET.get('num_decdet', None)
        prorogation_args['importateur'] = self.request.GET.get('importateur', None)
        prorogation_args['declarant'] = self.request.GET.get('declarant', None)
        prorogation_args['val_marchandise'] = self.request.GET.get('val_marchandise', None)
        prorogation_args['taux_suspension'] = self.request.GET.get('taux_suspension', None)
        prorogation_args['montant_dit'] = self.request.GET.get('montant_dit', None)
        prorogation_args['numero_caution'] = self.request.GET.get('numero_caution', None)
        prorogation_args['date_caution'] = self.request.GET.get('date_caution', None)
        prorogation_args['num_contrat'] = self.request.GET.get('num_contrat', None)
        prorogation_args['objet_contrat'] = self.request.GET.get('objet_contrat', None)
        prorogation_args['lieu_execution'] = self.request.GET.get('lieu_execution', None)
        prorogation_args['duree_contrat'] = self.request.GET.get('duree_contrat', None)
        prorogation_args['num_autors_pr'] = self.request.GET.get('num_autors_pr', None)
        prorogation_args['date_autors_pr'] = self.request.GET.get('date_autors_pr', None)
        prorogation_args['duree_decdet'] = self.request.GET.get('duree_decdet', None)
        articlesPks = self.request.GET.get('articlesPks', None)

        prorogation = ProrogationModel.objects.create_prorogation(prorogation_args)
        if prorogation and articlesPks:
            res = ArticleModel.objects.proroger_articles(articlesPks.split(','), prorogation)
            if res:
                return Response({"message" : "Prorogation ajoutée avec succès"}, status = 200)
        return Response({"message" : "Impossible de créer une prorogation avec ces données en entrée"}, status = 400 )


class ProrogationListAPIView(generics.ListAPIView):
    """docstring for DecDetListAPIView."""
    serializer_class    = ProrogationListSerializer
    permission_classes  = [permissions.IsAuthenticated]
    pagination_class    = ProrogationListPagination

    def get_queryset(self, *args, **kwargs):
        decdet_pk = self.kwargs.get('decdet_pk')
        queryset                = ProrogationModel.objects.filter(decdet = decdet_pk).order_by("-timestamp")

        query_code              = self.request.GET.get("code",None)
        query_num_decdet        = self.request.GET.get("num_decdet",None)
        query_appurement        = self.request.GET.get("appurement", None)
        query_importateur       = self.request.GET.get("importateur",None)
        query_declarant         = self.request.GET.get("declarant", None)
        query_taux_suspension   = self.request.GET.get("taux_suspension",None)
        query_numero_caution    = self.request.GET.get("numero_caution", None)
        query_numero_contrat    = self.request.GET.get("numero_contrat", None)
        query_objet_contrat     = self.request.GET.get("objet_contrat", None)
        query_lieu_execution    = self.request.GET.get("lieu_execution", None)
        query_num_autors_pr     = self.request.GET.get("num_autors_pr", None)
        query_year              = self.request.GET.get("year", None)
        query_month             = self.request.GET.get("month", None)

        if query_num_decdet is not None:
            queryset = queryset.filter(Q(num_decdet__icontains = query_num_decdet))
        if query_importateur is not None:
            queryset = queryset.filter(Q(importateur__icontains = query_importateur))
        if query_code is not None:
            queryset = queryset.filter(Q(code = query_code))
        if query_taux_suspension is not None:
            queryset = queryset.filter(Q(taux_suspension = query_taux_suspension))
        if query_declarant is not None:
            queryset = queryset.filter(Q(declarant__icontains = query_declarant))
        if query_objet_contrat is not None:
            queryset = queryset.filter(Q(objet_contrat__icontains = query_objet_contrat))
        if query_lieu_execution is not None:
            queryset = queryset.filter(Q(lieu_execution__icontains = query_lieu_execution))
        if query_numero_caution is not None:
            queryset = queryset.filter(Q(numero_caution__icontains = query_numero_caution))
        if query_num_autors_pr is not None:
            queryset = queryset.filter(Q(num_autors_pr__icontains = query_num_autors_pr))
        if query_year is not None:
            queryset = queryset.filter(Q(date_autors_pr__year = query_year))
        if query_month is not None:
            queryset = queryset.filter(Q(date_autors_pr__month = query_month))
        last_queryset = queryset
        if query_appurement is not None:
            last_queryset = []
            for decdet in queryset:
                if decdet.getDecdetStatus() == query_appurement:
                    last_queryset.append(decdet)
        prorogationQuerysetToCSV(last_queryset)
        return last_queryset
