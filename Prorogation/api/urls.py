from django.urls import path, include

from .views import ProrogationDecdetAPIView, ProrogationListAPIView

app_name = 'api-prorogation'

urlpatterns = [
    path('create/', ProrogationDecdetAPIView.as_view(), name = 'create'),
    path('list/<int:decdet_pk>', ProrogationListAPIView.as_view(), name = 'list'),
]
