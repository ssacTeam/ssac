from django.contrib import admin

# Register your models here.
from .models import ProrogationModel
admin.site.register(ProrogationModel)
