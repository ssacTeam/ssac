from django.contrib.staticfiles.templatetags.staticfiles import static

DecDetListCSVPath = ('static/CSV/Declarations_en_Detail.csv')
ArticleListCSVPath = ('static/CSV/Articles.csv')
ProrogationListCSVPath = ('static/CSV/Prorogations.csv')



def decdetQuerysetToCSV(queryset):
    csvFile = open(DecDetListCSVPath, 'w+')
    csvFile.write('CODE; N° Déclaration; Importateur; Déclarant; Valeur marchandise; Taux de suspension; Montant D/T; N° Caution; Date de caution; N° Contrat; Objet contrat; Lieu d\'exécution; Durée contrat; N° Autorisation Admission; Date Autorisation Admission; Durée accordée; Nombre d\'articles; Date Système\n')
    for decdet in queryset:

        csvFile.write('{};'.format(decdet.code))
        csvFile.write('{};'.format(decdet.num_decdet))
        csvFile.write('{};'.format(decdet.importateur))
        csvFile.write('{};'.format(decdet.declarant))
        csvFile.write('{};'.format(decdet.val_marchandise))
        csvFile.write('{};'.format(decdet.taux_suspension))
        csvFile.write('{};'.format(decdet.montant_dit))
        csvFile.write('{};'.format(decdet.numero_caution))
        csvFile.write('{};'.format(decdet.date_caution.strftime('%d %B %Y')))
        csvFile.write('{};'.format(decdet.num_contrat))
        csvFile.write('{};'.format(decdet.objet_contrat))
        csvFile.write('{};'.format(decdet.lieu_execution))
        csvFile.write('{} mois;'.format(decdet.duree_contrat))
        csvFile.write('{};'.format(decdet.num_autors_at))
        csvFile.write('{};'.format(decdet.date_autors_at.strftime('%d %B %Y')))
        csvFile.write('{} mois;'.format(decdet.duree_decdet))
        csvFile.write('{};'.format(len(decdet.article_set.all())))
        csvFile.write('{}\n'.format(decdet.timestamp.strftime('%d %B %Y à %H:%M')))


def articleQuerysetToCSV(queryset):
    csvFile = open(ArticleListCSVPath, 'w+')
    csvFile.write('N° l\'Admission temporaire;N° Série;Position tarifaire;Valeur (DA);Qte;Déscription; Appurement\n')
    for article in queryset:

        csvFile.write('{};'.format(article.decdet.num_decdet))
        csvFile.write('{};'.format(article.num_serie))
        csvFile.write('{};'.format(article.position_tarifaire))
        csvFile.write('{};'.format(article.valeur))
        csvFile.write('{};'.format(article.quantite))
        csvFile.write('{};'.format(article.description))
        csvFile.write('{}\n'.format(article.getAppure()))

def prorogationQuerysetToCSV(queryset):
    csvFile = open(ProrogationListCSVPath, 'w+')
    csvFile.write('CODE; N° Déclaration; N°AT; N°Déclaration mère; Importateur; Déclarant; Valeur marchandise; Taux de suspension; Montant D/T; N° Caution; Date de caution; N° Contrat; Objet contrat; Lieu d\'exécution; Durée contrat; N° Autorisation Prorogation; Date Autorisation Prorogation; Durée accordée; Nombre d\'articles; Date Système\n')
    for decdet in queryset:

        csvFile.write('{};'.format(decdet.code))
        csvFile.write('{};'.format(decdet.num_decdet))
        csvFile.write('{};'.format(decdet.decdet.num_decdet))
        if decdet.prorogation_mere:
            csvFile.write('{};'.format(decdet.prorogation_mere.num_decdet))
        else:
            csvFile.write('{};'.format(decdet.decdet.num_decdet))
        csvFile.write('{};'.format(decdet.importateur))
        csvFile.write('{};'.format(decdet.declarant))
        csvFile.write('{};'.format(decdet.val_marchandise))
        csvFile.write('{};'.format(decdet.taux_suspension))
        csvFile.write('{};'.format(decdet.montant_dit))
        csvFile.write('{};'.format(decdet.numero_caution))
        csvFile.write('{};'.format(decdet.date_caution.strftime('%d %B %Y')))
        csvFile.write('{};'.format(decdet.num_contrat))
        csvFile.write('{};'.format(decdet.objet_contrat))
        csvFile.write('{};'.format(decdet.lieu_execution))
        csvFile.write('{} mois;'.format(decdet.duree_contrat))
        csvFile.write('{};'.format(decdet.num_autors_pr))
        csvFile.write('{};'.format(decdet.date_autors_pr.strftime('%d %B %Y')))
        csvFile.write('{} mois;'.format(decdet.duree_decdet))
        csvFile.write('{};'.format(len(decdet.article_set.all())))
        csvFile.write('{}\n'.format(decdet.timestamp.strftime('%d %B %Y à %H:%M')))
