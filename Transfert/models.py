from django.db import models
from django.urls import reverse_lazy
import datetime

from DecDet.models import DecDetModel
from Prorogation.models import ProrogationModel
from Article.models import ArticleModel

class TransfertManager(models.Manager):
    def create_transfert(self, transfert_args):
        transfert = None
        articles_pks = transfert_args['articlesPks']
        if articles_pks is not None:
            try:
                decdet = DecDetModel.objects.get(num_decdet = transfert_args['decdet'])
                try:
                    prorogation = ProrogationModel.objects.get(num_decdet = transfert_args['prorogation'])
                except ProrogationModel.DoesNotExist:
                    prorogation = None
                transfert = self.model(
                    decdet = decdet,
                    prorogation = prorogation,
                    num_contrat = transfert_args['num_contrat'],
                    objet_contrat = transfert_args['objet_contrat'],
                    lieu_execution = transfert_args['lieu_execution'],
                    duree_contrat = transfert_args['duree_contrat'],
                    num_autors_tr = transfert_args['num_autors_tr'],
                    date_autors_tr = transfert_args['date_autors_tr'],
                    objet_autors_tr = transfert_args['objet_autors_tr'],
                    duree_transfert = transfert_args['duree_transfert']
                )
                transfert.save()
                for article_pk in articles_pks:
                    # article = ArticleModel.objects.get(pk = article_pk)
                    transfert.articles.add(article_pk)
            except Exception as e:
                print(e)
        return transfert

    def getApiListUrl(self):
        return reverse_lazy('transfert:api-transfert:list')

    def getArticleSet(self, transfert_pk):
        transfert = TransfertModel.objects.get(pk = transfert_pk)
        return transfert.getArticleSet()

    def getCreateAPIUrl(self):
        return reverse_lazy('transfert:api-transfert:create')



class TransfertModel(models.Model):
    """docstring for TransfertModel."""

    class Meta:
        verbose_name_plural = 'Transferts'

    decdet              = models.ForeignKey(DecDetModel, default=1, on_delete=models.CASCADE, related_name='transfer_set')
    prorogation         = models.ForeignKey(ProrogationModel, blank=True, null=True, on_delete=models.CASCADE, related_name='transfer_set')

    articles            = models.ManyToManyField(ArticleModel, related_name = 'transfert_set', blank=True)

    num_contrat         = models.CharField(max_length = 255, default='')
    objet_contrat       = models.CharField(max_length = 255, default='')
    lieu_execution      = models.CharField(max_length = 255, default='')
    duree_contrat       = models.PositiveIntegerField(default=0)

    num_autors_tr       = models.CharField(max_length = 255, default='')
    date_autors_tr      = models.DateField(default=datetime.date.today)
    objet_autors_tr     = models.CharField(max_length = 255, default='')
    duree_transfert     = models.PositiveIntegerField(default=0)

    timestamp           = models.DateTimeField(auto_now_add=True, blank=True)# date of creation


    objects = TransfertManager()

    def __str__(self):
        return "Transfert n°"+str(self.pk)

    def getDetailUrl(self):
        return reverse_lazy('transfert:detail', kwargs = {'pk':self.pk})

    def getUpdateUrl(self):
        return reverse_lazy('transfert:update', kwargs = {'pk':self.pk})

    def getArticleSet(self):
        return self.articles.all()
