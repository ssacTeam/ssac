from django import forms

from .models import TransfertModel

class TransfertUpdateForm(forms.ModelForm):
    class Meta:
        model = TransfertModel
        fields = '__all__'
