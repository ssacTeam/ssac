from rest_framework import pagination



class TransfertListPagination(pagination.PageNumberPagination):
    page_size           = 20
    page_query_param    = 'num_page'
