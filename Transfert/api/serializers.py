from rest_framework import serializers
from ..models import TransfertModel
from Article.api.serializers import ArticleSerializer
from DecDet.api.serializers import DecDetDetailSerializer


class TransfertSerializer(serializers.ModelSerializer):
    transfert_detail_url        = serializers.SerializerMethodField()
    transfert_update_url        = serializers.SerializerMethodField()
    date                        = serializers.SerializerMethodField()
    articles = ArticleSerializer(many=True)
    decdet_from = DecDetDetailSerializer()
    decdet_to = DecDetDetailSerializer()

    class Meta:
        model = TransfertModel
        fields = [
            'pk',
            'decdet_from',
            'decdet_to',
            'articles',
            'date',
            'timestamp',

            'transfert_detail_url',
            'transfert_update_url',
        ]

    def get_articles(self, object):
        return object.getArticleSet()

    def get_transfert_detail_url(self, object):
        return object.getDetailUrl()

    def get_transfert_update_url(self, object):
        return object.getUpdateUrl()

    def get_date(self, object):
        return object.date.strftime('%d %B %Y')

class TransfertListSerializer(TransfertSerializer):
    pass
