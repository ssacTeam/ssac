from rest_framework import generics, permissions
from rest_framework.views import APIView
from rest_framework.response import Response

from .pagination import TransfertListPagination
from .serializers import TransfertSerializer, TransfertListSerializer

from ..models import TransfertModel

class TransfertListAPIView(generics.ListAPIView):
    serializer_class = TransfertSerializer
    pagination_class = TransfertListPagination
    permission_classes  = [permissions.IsAuthenticated]

    def get_queryset(self, *args, **kwargs):
        queryset = TransfertModel.objects.all().order_by("-timestamp")
        return queryset


class TransfertCreateAPIView(APIView):
    """docstring for ArticleTransfertAPIView."""
    def get(self, request, format = None):
        transfert_args = {}
        transfert_args['decdet'] = self.request.GET.get('decdet', None)
        transfert_args['prorogation'] = self.request.GET.get('prorogation', None)
        transfert_args['articlesPks'] = self.request.GET.get('articlesPks', None)
        transfert_args['num_contrat'] = self.request.GET.get('num_contrat', None)
        transfert_args['objet_contrat'] = self.request.GET.get('objet_contrat', None)
        transfert_args['lieu_execution'] = self.request.GET.get('lieu_execution', None)
        transfert_args['duree_contrat'] = self.request.GET.get('duree_contrat', None)
        transfert_args['num_autors_tr'] = self.request.GET.get('num_autors_tr', None)
        transfert_args['date_autors_tr'] = self.request.GET.get('date_autors_tr', None)
        transfert_args['objet_autors_tr'] = self.request.GET.get('objet_autors_tr', None)
        transfert_args['duree_transfert'] = self.request.GET.get('duree_transfert', None)
        articlePks = transfert_args['articlesPks']

        if articlePks:
            articlePks = articlePks.split(',')
            transfert = TransfertModel.objects.create_transfert(transfert_args)
            if transfert:
                return Response({"message" : "Transfert effectué avec succès"}, status = 200)
            return Response({"message" : "4"}, status = 400 )
        return Response({"message" : "Impossible de transférer d'articles avec ces données en entrée"}, status = 400 )
