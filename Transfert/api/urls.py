from django.urls import path, include

from .views import TransfertListAPIView, TransfertCreateAPIView

app_name = 'api-transfert'


urlpatterns = [
    path('list/', TransfertListAPIView.as_view(), name = 'list'),
    path('create/', TransfertCreateAPIView.as_view(), name = 'create'),
]
