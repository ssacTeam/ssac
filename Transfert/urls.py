from django.urls import path, include

from .views import TransfertDetailView, TransfertListView, TransfertUpdateView, TransfertDecdetListView

app_name = 'transfert'

urlpatterns = [
    path('list/', TransfertListView.as_view(), name = 'list'),
    path('list/<int:pk>', TransfertDecdetListView.as_view(), name = 'decdet-list'),
    path('<int:pk>/', TransfertDetailView.as_view(), name = 'detail'),
    path('update/<int:pk>', TransfertUpdateView.as_view(), name = 'update'),

    path('api/', include('Transfert.api.urls', namespace='api-transfert'))
]
