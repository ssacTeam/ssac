from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.views import generic

from .forms import TransfertUpdateForm
from .models import TransfertModel

from Prorogation.models import ProrogationModel
from DecDet.models import DecDetModel

from CSV.CSV import articleQuerysetToCSV

class TransfertListView(generic.ListView, LoginRequiredMixin):
    model = TransfertModel
    template_name = "transfert_list.html"

    def get_queryset(self, *args, **kwargs):
        queryset = TransfertModel.objects.all()
        return queryset

    def get_context_data(self, *args, **kwargs):
        context = super(TransfertListView, self).get_context_data()
        context['transfert_api_list_url'] = TransfertModel.objects.getApiListUrl()
        return context


class TransfertDecdetListView(generic.ListView, LoginRequiredMixin):
    model = TransfertModel
    template_name = "transfert_decdet_list.html"

    def get_queryset(self, *args, **kwargs):
        queryset = None
        pk = self.kwargs.get('pk')
        try:
            at = DecDetModel.objects.get(pk = pk)
            queryset = at.transfer_set.all()
        except DecDetModel.DoesNotExist:
            try:
                prorogation = ProrogationModel.objects.get(pk = pk)
                queryset = prorogation.decdet.transfer_set.all()
            except ProrogationModel.DoesNotExist:
                pass
        return queryset.order_by('-timestamp')

    def get_context_data(self, *args, **kwargs):
        pk = self.kwargs.get('pk')
        at = None
        try:
            at = DecDetModel.objects.get(pk = pk)
        except DecDetModel.DoesNotExist:
            try:
                prorogation = ProrogationModel.objects.get(pk = pk)
                at = prorogation.decdet
            except ProrogationModel.DoesNotExist:
                pass
        context = super(TransfertDecdetListView, self).get_context_data()
        context['decdet']     = at
        return context




class TransfertDetailView(generic.DetailView, LoginRequiredMixin):
    model = TransfertModel
    template_name = "transfert_detail.html"

    def get_object(self, *args, **kwargs):
        transfert_pk = self.kwargs.get('pk')
        transfert = get_object_or_404(TransfertModel, pk = transfert_pk)
        articleQuerysetToCSV(transfert.articles.all())
        return transfert

    def get_context_data(self, *args, **kwargs):
        transfert_pk = self.kwargs.get('pk')
        transfert = TransfertModel.objects.get(pk = transfert_pk)
        context = super(TransfertDetailView, self).get_context_data()
        context['date_creation']        = transfert.timestamp.strftime('%A %d %B %Y')
        context['temps_creation']       = transfert.timestamp.strftime('%H:%M:%S')

        return context


class TransfertUpdateView(generic.UpdateView, LoginRequiredMixin):
    form_class = TransfertUpdateForm
    template_name = "transfert_update.html"

    def get_object(self, *args, **kwargs):
        transfert_pk = self.kwargs.get('pk')
        transfert = get_object_or_404(TransfertModel, pk = transfert_pk)
        return transfert
