from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.views import generic

from .models import AppurementModel
from DecDet.models import DecDetModel

from CSV.CSV import articleQuerysetToCSV


class AppurementListView(generic.ListView, LoginRequiredMixin):
    model = AppurementModel
    template_name = "appurement_decdet_list.html"

    def get_queryset(self, *args, **kwargs):
        decdet_pk = self.kwargs.get('decdet_pk')
        queryset  = AppurementModel.objects.filter(decdet = decdet_pk)
        return queryset

    def get_context_data(self, *args, **kwargs):
        decdet_pk = self.kwargs.get('decdet_pk')
        decdet = get_object_or_404(DecDetModel, pk = decdet_pk)
        context = super(AppurementListView, self).get_context_data()
        context['decdet'] = decdet
        return context


class AppurementDetailView(generic.DetailView, LoginRequiredMixin):
    model = AppurementModel
    template_name = "appurement_detail.html"

    def get_object(self, *args, **kwargs):
        object = super(AppurementDetailView, self).get_object()
        articleQuerysetToCSV(object.articles.all())
        return object

    # def get_context_data(self, *args, **kwargs):
    #     decdet_pk = self.kwargs.get('decdet_pk')
    #     decdet = AppurementModel.objects.get(pk = decdet_pk)
    #     context = super(AppurementListView, self).get_context_data()
    #     context['decdet'] = decdet
    #     return context
