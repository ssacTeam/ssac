from django.db import models
from django.urls import reverse_lazy
import datetime

from DecDet.models import DecDetModel
from Article.models import ArticleModel

class AppurementManager(models.Manager):
    def create_apurement(self, appurement_args):
        appurement = None
        create_appurement = True
        articles_pks = appurement_args['articlesPks']
        for article_pk in articles_pks:
            article = ArticleModel.objects.get(pk = article_pk)
            if article.appure:
                create_appurement = False
        if create_appurement:
            #Apurement par déclaration
            try:
                print(appurement_args['decdet'])
                decdet = DecDetModel.objects.get(num_decdet = appurement_args['decdet'])
                if appurement_args['num_decdet']:
                    appurement = self.model(
                        decdet = decdet,
                        date_appurement = appurement_args['date_appurement'],
                        objet_appurement = appurement_args['objet_appurement'],
                        code = appurement_args['code'],
                        num_decdet = appurement_args['num_decdet'],
                        importateur = appurement_args['importateur'],
                        declarant = appurement_args['declarant'],
                        val_marchandise = appurement_args['val_marchandise'],
                        taux_suspension = appurement_args['taux_suspension'],
                        montant_dit = appurement_args['montant_dit'],
                        num_appurement = appurement_args['num_appurement']
                    )
                    appurement.save()
                #Apurement par décision
                else:
                    appurement = self.model(
                        decdet = decdet,
                        date_appurement = appurement_args['date_appurement'],
                        objet_appurement = appurement_args['objet_appurement'],
                        num_decision = appurement_args['num_decision'],
                        num_appurement = appurement_args['num_appurement']
                    )
                    appurement.save()
                for article_pk in articles_pks:
                        article = ArticleModel.objects.get(pk = article_pk)
                        article.apurer()
                        appurement.articles.add(article)
            except (DecDetModel.DoesNotExist, ArticleModel.DoesNotExist) as e:
                print(e)
            return appurement

    def getApiListUrl(self):
        return reverse_lazy('appurement:api-appurement:list')

    def getArticleSet(self, appurement_pk):
        appurement = AppurementModel.objects.get(pk = appurement_pk)
        return appurement.getArticleSet()



class AppurementModel(models.Model):
    """docstring for AppurementModel."""

    class Meta:
        verbose_name_plural = 'Appurements'

    decdet          = models.ForeignKey(DecDetModel, default=1, on_delete=models.CASCADE, related_name='appurement_set')
    articles        = models.ManyToManyField(ArticleModel, related_name = 'appurement_set')
    date_appurement = models.DateField(default=datetime.date.today)
    objet_appurement= models.TextField(max_length=255)
    num_appurement  = models.CharField(max_length = 255, default='', blank = True)

    num_decision    = models.CharField(max_length = 255, null = True, blank = True)

    code            = models.CharField(max_length = 4, default='', null = True, blank = True)
    num_decdet      = models.CharField(max_length = 255, default='',  null = True, blank = True)
    importateur     = models.CharField(max_length = 255, default='', null = True, blank = True)
    declarant       = models.CharField(max_length = 255, default='', null = True, blank = True)
    val_marchandise = models.PositiveIntegerField(default=0, null = True, blank = True)
    taux_suspension = models.PositiveIntegerField(default=0, null = True, blank = True)
    montant_dit     = models.PositiveIntegerField(default=0, null = True, blank = True)



    timestamp       = models.DateTimeField(auto_now_add=True, blank=True)# date of creation


    objects = AppurementManager()

    def __str__(self):
        return "Apurement n°"+str(self.num_appurement)

    def getDetailUrl(self):
        return reverse_lazy('appurement:detail', kwargs = {'pk':self.pk})

    def getArticleSet(self):
        return self.articles.all()

    def getDetailUrl(self):
        return reverse_lazy('appurement:detail', kwargs={'pk':self.pk})
