from django.urls import path

from .views import AppurementListView, AppurementDetailView

app_name = 'Appurement'

urlpatterns = [
    path('list/<int:decdet_pk>', AppurementListView.as_view(), name = 'decdet-list'),
    path('<int:pk>', AppurementDetailView.as_view(), name = 'detail')
]
