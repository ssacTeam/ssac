
function demo(){
  at_Year_Axis_Y = [ 1065, 946, 2400, 790];
  at_Apure_Axis_Y1 = at_Year_Axis_Y;
  at_Apure_Axis_Y2 = [ 1024, 1680, 2000, 30];
}


function at_year_chart() {
    var canvas = $('#at_year');
    var chart = new Chart(canvas, {
      type: 'bar',
      data: {
        labels: at_Year_Axis_X,
        datasets: [{
          label: 'Nombre d\'ATs',
          data: at_Year_Axis_Y,
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(38, 239, 75, 0.2)',
            'rgba(255, 206, 86, 0.2)',
          ],
          borderColor: [
            'rgba(255,99,132,1)',
            'rgba(75, 192, 192, 1)',
            'rgba(38, 239, 75, 1)',
            'rgba(255, 206, 86, 1)',
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
}


function at_apure_chart() {
    var canvas = $('#at_apure');
    var chart = new Chart(canvas, {
      type: 'line',
      data: {
        labels: at_Apure_Axis_X,
        datasets: [
          {
            label: 'Nombre d\'ATs',
            data: at_Apure_Axis_Y1,
            backgroundColor: [
              'rgba(255, 206, 86, 0.2)',
            ],
            borderColor: [
              'rgba(255, 206, 86, 1)',
            ],
            borderWidth: 1
          },
          {
            label: 'Nombre d\'ATs apurés',
            data: at_Apure_Axis_Y2,
            backgroundColor: [
              'rgb(38, 239, 75, 0.2)',
            ],
            borderColor: [
              'rgb(38, 239, 75, 1)',
            ],
            borderWidth: 1
          }
        ]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
}
