var fetchDataUrl = $('#settings').attr('transfert_api_list_url');
var transfertListHtmlContainer = $('#transfert-List-Html-Container');
var query = '';
var transfertList = '';
var nextFetchDataUrl = '';
var previousFetchDataUrl = '';
var transfertCount = '';
var transfertListPageNum = 1
const PAGINATION = 20
function init() {
  fetchData();
  listeners();
}

function listeners() {

}

function fetchData() {
  $.ajax({
       type       : "GET",
       url        : fetchDataUrl,
       contentType: 'application/json',
       data       : query,
       success    : function(data){
         transfertList              = data.results;
         nextFetchDataUrl        = data.next;
         previousFetchDataUrl    = data.previous;
         transfertCount             = data.count;
         parseTransferts();
         updateDataNavigationBtns();
       },
       error      : function(data){
         console.log("ERROR:Transfert0x1 while fetching");
         console.log("data :",data.status, data.statusText);
       }
     });
}

//decDetList in JSON
function parseTransferts() {
  if (transfertList == 0) {
    displayEmpty();
  } else {
    $.each(transfertList, function(key,object){
        attachTransfert(object);
      });
  }
}


function displayEmpty() {
  transfertListHtmlContainer.text('Aucun transfert à afficher');
}

function attachTransfert(transfert) {
  var tr = $('<tr></tr>');
  var thDecdetFrom = $('<td></td>').text(transfert.decdet_from.reference);
  var thDecdetTo = $('<td></td>').text(transfert.decdet_to.reference);
  var thNbArticles = $('<td></td>').text(transfert.articles.length);
  var thDate = $('<td></td>').text(transfert.date);
  var aDetail  = $('<a></a>').append('<i class="fas fa-angle-double-right"></i>')
                .attr({
                  href : transfert.transfert_detail_url,
                  'data-toggle' : "tooltip",
                  'data-placement' : "left",
                   title : "Détail du transfert"
                 });
  var thActions = $('<td></td>').append(aDetail);
  tr.append(thDecdetFrom, thDecdetTo ,thDate, thNbArticles, thActions);
  transfertListHtmlContainer.append(tr);
}

function refetchData() {
    transfertListHtmlContainer.text("");
    fetchData();
}


function updateDataNavigationBtns() {
    var nextDataNavigationBtn = $('#navigation-fetch-data-btn-next');
    var previousDataNavigationBtn = $('#navigation-fetch-data-btn-previous');
    if(nextFetchDataUrl == null) nextDataNavigationBtn.attr('disabled', true);
    else nextDataNavigationBtn.attr('disabled', false);
    if(previousFetchDataUrl == null) previousDataNavigationBtn.attr('disabled', true);
    else previousDataNavigationBtn.attr('disabled', false);
}

function updateDecDetsInfo() {
    $('#transfert-list-html-info').text(
            'Affichage de '+ transfertList.length +' transfert(s) sur '+ transfertCount +
            ' | Page n°' + transfertListPageNum + ' / ' + (parseInt(transfertCount / PAGINATION)+1)
    );
}


//Listeners

function listeners() {

    $('#navigation-fetch-data-btn-next').on('click', function(){
        fetchDataUrl = nextFetchDataUrl;
        refetchData();
        transfertListPageNum++;
    });

    $('#navigation-fetch-data-btn-previous').on('click', function(){
        fetchDataUrl = previousFetchDataUrl;
        refetchData();
        transfertListPageNum--;
    });
}
