var prorogationCreateUrl =  $('#settings').attr('prorogation_create_api_url');
var prorogationCreateForm = $('#prorogation-create-form');


function prorogation_create_modal_init() {
  date = new Date().toISOString().substr(0, 10);
  prorogationCreateForm.find('input[name=date_prorogation]').val(date);
  prorogation_create_modal_listeners();
}


function prorogation_create_modal_listeners() {

  $('#prorogation-create-form-submit-btn').on('click', function(event) {
    event.preventDefault();
    $('#prorogation-create-form').find('[type=submit]').trigger('click');
  })


  $('#prorogation-create-form').on('submit', function(event){
    event.preventDefault();
    var prorogation_duree = $(this).find('input[name=duree_added]').val();
    var prorogation_duree_max = $(this).find('input[name=duree_added]').attr('max');
    if(prorogation_duree <= prorogation_duree_max){
      serialized_form = $(this).serialize();
      creerProrogation(serialized_form);
    }
    else {
      alert('La prorogation dépasse la durée du contrat');
    }
  });


}


function creerProrogation(data) {

  $('#prorogation-create-modal').modal('hide');

  $.ajax({
       type       : "GET",
       url        : prorogationCreateUrl,
       contentType: 'application/json',
       data       : data,
       success    : function(data){
         alert("Prorogation effectuée avec succès");
         location.reload()
       },
       error      : function(data){
         console.log("ERROR:DecDet0x6 while creating prorogation");
         console.log("data :",data.status, data.statusText);
       }
     });
}
