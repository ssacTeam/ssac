var fetchArticleListUrl = $('#settings').attr('decdet_articles_url');
var createArticleListUrl = $('#settings').attr('create_article_api_url');
var decdetArticlesTbody  = $('#decdet-articles-tbody');
var nbArticlesSpan = $('#nb-articles-span');

function init() {
    $('.must-select-article').attr('disabled', true);
    getArticleList();
    initArticleTBody();
    listeners();
}


function getArticleList() {
    fetchArticleList(fetchArticleListUrl)
}

function fetchArticleList(fetchDataUrl) {
  $.ajax({
       type       : "GET",
       url        : fetchDataUrl,
       contentType: 'application/json',
       success    : function(data){
         initArticleTBody(data);
       },
       error      : function(data){
         console.log("ERROR:DecDet0x5 while fetching Articles");
         console.log("data :",data.status, data.statusText);
       }
     });
}


function initArticleTBody(articles) {
  if(articles != null){
    decdetArticlesTbody.text('');
    nbArticlesSpan.text(articles.length)
    if(parseInt(articles.length) == 0){
      $('#article-csv-btn').hide()
      decdetArticlesTbody.parent().after('<h5><i>Aucun article n\'est associé à cette déclaration</i></h5>');
    }
    $.each(articles, function(key,article){
        var articleHTML = parseArticle(article);
        decdetArticlesTbody.append(articleHTML);
    });
  }
}

function parseArticle(article) {
    var status_appurement = article.status_appurement;
    var delta_appurement = article.delta_appurement;
    switch (status_appurement) {
      case 'appure':  istatus = $('<i></i>').addClass(
                'appurement-appure fas fa-check-circle');
        break;
      case 'pending': istatus = $('<i></i>').addClass(
                'appurement-pending fas fa-stopwatch').text('  '+delta_appurement+' mois');
        break;
      case 'retard':  istatus = $('<i></i>').addClass(
                'appurement-retard fas fa-times-circle').text('  '+ (-delta_appurement)+' mois');
        break;
    }

    var trArticle = $('<tr></tr>');
    var checkBoxSelect = $('<input>').attr({
      type : "checkbox",
      pk : article.pk
    }).addClass('checkbox-select-article');
    var tdSelect = $('<td></td>').append(checkBoxSelect);
    var tdNumSerie = $('<td></td>').text(article.num_serie);
    var tdPositionTarif = $('<td></td>').text(article.position_tarifaire);
    var tdValeur = $('<td></td>').text(article.valeur);
    var tdQuantite = $('<td></td>').text(article.quantite);
    var tdDesctiption = $('<td></td>').text(article.description);
    var tdStatus = $('<td></td>').append(istatus);
    var tdAction = $('<td></td>');
    var aDetailArticle = $('<a></a>').attr({
                href : article.article_update_url,
                'data-toggle' : "tooltip",
                'data-placement' : "left",
                 title : "Modifier l'article"
              }).append('<i class="fas fa-pen"></i>');
    tdAction.append(aDetailArticle);
    if (article.transfertBool){
      var aDetailTransfert = $('<a></a>').attr({
                  style : "color:green",
                  href : article.lastTransfertUrl,
                  'data-toggle' : "tooltip",
                  'data-placement' : "left",
                   title : "Détail du transfert"
                }).append('<i class="fas fa-location-arrow"></i>');
      tdAction.append(' ', aDetailTransfert);
    }
    trArticle.append(tdSelect, tdNumSerie, tdPositionTarif, tdValeur, tdQuantite, tdDesctiption, tdStatus, tdAction);
    return trArticle;
}

function createArticle(data) {
  $('#article-create-modal').modal('hide');
  $.ajax({
       type       : "GET",
       url        : createArticleListUrl,
       contentType: 'application/json',
       data       : data,
       success    : function(data){
         updateArticleTBody(data);
       },
       error      : function(data){
         console.log("ERROR:DecDet0x5 while creating Article");
         console.log("data :",data.status, data.statusText);
       }
     });
}

function updateArticleTBody(articleData) {
  if (articleData) {
    var articleHTML = parseArticle(articleData);
    decdetArticlesTbody.append(articleHTML);
  }
}



function listeners() {
  $('#create-article-btn').click(function(event){
    event.preventDefault();
    $('#article-create-modal').modal();
  });

  $('#transferer-article-btn').click(function(event){
    event.preventDefault();
    $('#article-transfert-modal').modal();
    var input = $('#article-transfert-modal').find('#article-transfert-form-input');
    input.val(0);
  });

  $('#appurement-article-btn').click(function(event){
    event.preventDefault();
    $('#article-appurement-modal').modal();
  });

  $('#prorogation-decdet-btn').click(function(event){
    event.preventDefault();
    $('#prorogation-create-modal').modal();
    valMin = $('#prorogation-create-modal').find('input[name=ancient-duree]').val();
    $('#prorogation-create-modal').find('input[name=new_duree]').val(valMin);
  });

  $('#article-create-form-submit-btn').click(function(event) {
    $('#article-create-form').find('[type=submit]').trigger('click');
  });

  $('#article-create-form').submit(function(event) {
    event.preventDefault();
    var form = $(this);
    var data = form.serialize();
    createArticle(data);
  });

  $('#article-checkbox-select-all').prop('checked', false)
  $('#article-checkbox-select-all').change(function(event) {
    this_ = $(this);
    if(this_.prop('checked'))
      $('.checkbox-select-article').prop('checked', false);
    else
      $('.checkbox-select-article').prop('checked', true);

    $('.checkbox-select-article').trigger('click');
  });

  $(document).on('change','.checkbox-select-article',  function() {
    nbCheckBoxsChecked = $('.checkbox-select-article:checked').length;
    if(nbCheckBoxsChecked >0 )
      $('.must-select-article').attr('disabled', false);
    else
      $('.must-select-article').attr('disabled', true);
  })

}
