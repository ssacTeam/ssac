var postDataUrl = $('#settings').attr('decdet_api_create_url');
var decdetListUrl  = $('#settings').attr('decdet_list_url');
var postData = {};


function createDecDet(form) {
  postData = form.serialize();

  $.ajax({
       method     : "GET",
       url        : postDataUrl,
       // dataType: "json",
       // contentType: "application/json",
       data       : postData,
       success    : function(data){
         alert('Déclaration en détail créée avec succès');
         next_or_back(form, data);
       },
       error      : function(data){
         console.log("ERROR:DecDet0x2 while creating");
         console.log("data :",data.status, data.statusText);
         $('#alert-danger').show('slow', function () {
           setTimeout(function functionName() {
             $('#alert-danger').hide('slow');
           }, 3000);
         });
       }
     });

}

function next_or_back(form, data) {
  var nextActionChoice = $('.next-action-radio:checked').val();
  switch (nextActionChoice) {
    case '1': location.replace(decdetListUrl);
      break;
    case '2': location.replace(data.decdet_detail_url);
      break;
    case '3': form.trigger('reset');
      break;
    default:
    console.log(nextActionChoice);

  }

}

function initCreateDecdet(){

  $('#create-decdet-form').on('submit', function(event){
      event.preventDefault();
      var form = $(this);
      createDecDet(form);
  });
}
//
// function getFormData($form){
//     var unindexed_array = $form.serializeArray();
//     var indexed_array = {};
//     $.map(unindexed_array, function(n, i){
//         indexed_array[n['name']] = n['value'];
//     });
//
//     return indexed_array;
// }

//
// function dateParse(dateP) {
//   var date = new Date(dateP);
//   var year  = date.getFullYear();
//   var month  = date.getMonth();
//   var day  = date.getDate();
//   return year+'-'+month+'-'+day;
//
// }

//
// function prepareFormJson() {
//   var form = $('#create-decdet-form');
//   var postFormData  = getFormData(form);
//   postData = {
//     code : postFormData['code'],
//     importateur : postFormData['importateur'],
//     declarant : postFormData['declarant'],
//     objet_contrat : postFormData['objet_contrat'],
//     lieu_execution : postFormData['lieu_execution'],
//     duree_contrat : postFormData['duree_contrat'],
//     taux_suspension : postFormData['taux_suspension'],
//     duree_decdet : postFormData['duree_decdet'],
//     montant_dit : postFormData['montant_dit'],
//     numero_caution : postFormData['numero_caution'],
//     date_caution : postFormData['date_caution'],
//   }
// }
