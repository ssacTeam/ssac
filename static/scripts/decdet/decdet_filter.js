var FILTER_TOGGLED = false

function decdetFilterInit() {
  decdetFilterListeners();
}

function decdetFilterListeners() {
    $('#decdet-filter-toggle-btn').on('click', function(event) {
        event.preventDefault();
        if(FILTER_TOGGLED){
          FILTER_TOGGLED = false
          $('#decdet-list-filter-form').slideUp();
        }else{
          FILTER_TOGGLED = true
          $('#decdet-list-filter-form').slideDown();
        }
    })
}
