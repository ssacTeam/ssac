const PAGINATION = $('#settings').attr('PAGE_SIZE');
var decDetListHtmlContainer = $('#decDet-List-Html-Container');
var fetchDataUrl = $('#settings').attr('decdet_api_list_url');
var decetList;
var nextFetchDataUrl;
var previousFetchDataUrl;
var decDetCount;
var decdetListPageNum = 1;
var query = [];

function init() {
  fetchData();
  listeners();
}

function fetchData() {
  $.ajax({
       type       : "GET",
       url        : fetchDataUrl,
       contentType: 'application/json',
       data       : query,
       success    : function(data){
         decDetList              = data.results;
         nextFetchDataUrl        = data.next;
         previousFetchDataUrl    = data.previous;
         decDetCount             = data.count;
         parseDecDets();
         updateDataNavigationBtns();
         updateDecDetsInfo();
       },
       error      : function(data){
         console.log("ERROR:DecDet0x1 while fetching");
         console.log("data :",data.status, data.statusText);
       }
     });
}

//decDetList in JSON
function parseDecDets() {
  if (decDetList == 0) {
    displayEmpty();
  } else {
    $.each(decDetList, function(key,object){
        var decDetKey      =key;
        attachDecDet(object);
      });
  }
}


function displayEmpty() {
  decDetListHtmlContainer.text('Aucune déclaration en détail à afficher');
}

function attachDecDet(decdet) {
  var status_appurement = decdet.status_appurement;
  var iStatus='';
  var textStatus='';
  var tdAppurement = $('<td></td>');
  switch (status_appurement) {
    case 'appure':  iStatus = $('<i></i>').addClass(
              'appurement-appure fas fa-check-circle');
              textStatus = ('  Apuré');
              tdAppurement.addClass('appurement-appure');
      break;
    case 'pending': iStatus = $('<i></i>').addClass(
              'appurement-pending fas fa-stopwatch');
              textStatus = ('  En cours');
              tdAppurement.addClass('appurement-pending');
      break;
    case 'retard':  iStatus = $('<i></i>').addClass(
              'appurement-retard fas fa-times-circle');
              textStatus = ('  En retard');
              tdAppurement.addClass('appurement-retard');
      break;
    case 'pending-retard':  iStatus = $('<i></i>').addClass(
              'appurement-pending-retard fas fa-exclamation-circle');
              textStatus = (' '+decdet.nb_articles_statut_retard+'  En retard');
              tdAppurement.addClass('appurement-pending-retard');
      break;
  }
  var tr = $('<tr></tr>');
  var tdCode = $('<td></td>').text(decdet.code);
  var tdReference = $('<td></td>').text(decdet.num_decdet);
  var tdImportateur = $('<td></td>').text(decdet.importateur);
  var tdDureeAcc = $('<td></td>').text(decdet.duree_decdet);
  var tdDureeContrat = $('<td></td>').text(decdet.duree_contrat);
  var aDetail  = $('<a></a>').append('<i class="fas fa-angle-double-right"></i>')
                      .attr({
                         href : decdet.decdet_detail_url,
                         'data-toggle' : "tooltip",
                         'data-placement' : "left",
                          title : "Détail de la déclaration"
                       });
  // var aUpdate  = $('<a></a>').text('Modifier').attr({ href : decdet.decdet_update_url });
  tdAppurement.append(iStatus, textStatus);
  var tdActions = $('<td></td>').append(aDetail);
  tr.append(tdCode, tdReference, tdImportateur, tdDureeAcc, tdDureeContrat, tdAppurement, tdActions);
  decDetListHtmlContainer.append(tr);
}


function refetchData() {
    decDetListHtmlContainer.text("");
    fetchData();
}

function updateDataNavigationBtns() {
    var nextDataNavigationBtn = $('#navigation-fetch-data-btn-next');
    var previousDataNavigationBtn = $('#navigation-fetch-data-btn-previous');
    if(nextFetchDataUrl == null) nextDataNavigationBtn.attr('disabled', true);
    else nextDataNavigationBtn.attr('disabled', false);
    if(previousFetchDataUrl == null) previousDataNavigationBtn.attr('disabled', true);
    else previousDataNavigationBtn.attr('disabled', false);
}

function updateDecDetsInfo() {
    $('#decdet-list-html-info').text(
            'Affichage de '+ decDetList.length +' déclaration(s) sur '+ decDetCount +
            ' | Page n°' + decdetListPageNum + ' / ' + (parseInt(decDetCount / PAGINATION)+1)
        );
}


//Listeners

function listeners() {

    $('#navigation-fetch-data-btn-next').on('click', function(){
        fetchDataUrl = nextFetchDataUrl;
        refetchData();
        decdetListPageNum++;
    });

    $('#navigation-fetch-data-btn-previous').on('click', function(){
        fetchDataUrl = previousFetchDataUrl;
        refetchData();
        decdetListPageNum--;
    });

    $('#decdet-list-filter-form').on('submit', function(event) {
        event.preventDefault();
        var formFields = $(this).find('input,select');
        query = [];
        $.each(formFields, function(key,object){
          if(object.name && object.value)
            query.push({
              name   : object.name,
              value  : object.value
            });
        });
        refetchData();
    });

    $('#decdet-list-filter-form').on('reset', function(event) {
        query = [];
        refetchData();
    });
}
