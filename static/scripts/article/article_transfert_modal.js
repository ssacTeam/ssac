var fetchDecdetDetailUrl =  $('#settings').attr('decdet_api_detail_url');
var transfertArticleUrl =  $('#settings').attr('transfert_article_api_url');
var currentDecdetPK =  $('#settings').attr('pk');
var decdetTransfertForm = $('#transfert-create-form');
var decdetDestinationRef = 0;


function article_transfert_modal_init() {
  // date = new Date().toISOString().substr(0, 10);
  // $('#article-transfert-form-date').val(date);
  article_transfert_modal_listeners();
}


function article_transfert_modal_listeners() {

  $('#article-transfert-form-btn').on('click', function(event) {
    event.preventDefault();
    $('#transfert-create-form').find('[type=submit]').trigger('click');

  });

  $('#transfert-create-form').on('submit', function(event){
    event.preventDefault();
    var articlesTableBody = $('#decdet-articles-tbody');
    var articleSelectCheckBoxs = articlesTableBody.find('input[type="checkbox"]:checked');
    var articlesPks = [];
    $.each(articleSelectCheckBoxs, function(){
        articlesPks.push($(this).attr('pk'));
    });
    var articlePksInput = $('<input>').attr({
      name : 'articlesPks',
      hidden : true
    }).val(articlesPks.toString());
    $(this).append(articlePksInput);

    var form = $(this);
    transferArticlesToDecdet(form);
  });


}

function transferArticlesToDecdet(form) {
  console.log(transfertArticleUrl);
  var data_to_send = form.serialize();
  $('#article-transfert-modal').modal('hide');

  $.ajax({
       type       : "GET",
       url        : transfertArticleUrl,
       contentType: 'application/json',
       data       : data_to_send,
       success    : function(data){
         alert("Article(s) transféré(s) avec succès");
         location.reload();
       },
       error      : function(data){
         console.log("ERROR:DecDet0x5 while transfering articles");
         console.log("data :",data.status, data.statusText);
         if( data.responseJSON.message==4){
           $('#alert-danger').find('label').text('Vous ne pouvez pas transférer un article vers la même déclaration');
           $('#alert-danger').show('slow', function () {
             setTimeout(function functionName() {
               $('#alert-danger').hide('slow');
             }, 3000);
           });

         }
       }
     });
}
