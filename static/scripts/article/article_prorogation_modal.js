var fetchDecdetDetailUrl =  $('#settings').attr('decdet_api_detail_url');
var prorogationArticleUrl =  $('#settings').attr('prorogation_create_api_url');
var currentDecdetPK =  $('#settings').attr('pk');
var decdetProrogationForm = $('#article-prorogation-form');


function article_prorogation_modal_init() {
  article_prorogation_modal_listeners();
}


function article_prorogation_modal_listeners() {

  $('#prorogation-create-form-submit-btn').on('click', function(event) {
    event.preventDefault();
    $('#prorogation-create-form').find('[type=submit]').trigger('click');
  });


  $('#prorogation-create-form').on('submit', function(event){
    event.preventDefault();
    var articlesTableBody = $('#decdet-articles-tbody');
    var articleSelectCheckBoxs = articlesTableBody.find('input[type="checkbox"]:checked');
    var articlesPks = [];
    $.each(articleSelectCheckBoxs, function(){
        articlesPks.push($(this).attr('pk'));
    });
    var articlePksInput = $('<input>').attr({
      name : 'articlesPks',
      hidden : true
    }).val(articlesPks.toString());
    $(this).append(articlePksInput);

    var form = $(this);
    prorogationArticles(form);
  });

}


function prorogationArticles(form) {
  var data_to_send = form.serialize();
  $('#article-prorogation-modal').modal('hide');

  $.ajax({
       type       : "GET",
       url        : prorogationArticleUrl,
       contentType: 'application/json',
       data       : data_to_send,
       success    : function(data){
         alert("Prorogation effectuée avec succès");
         location.reload();
       },
       error      : function(data){
         console.log("ERROR:DecDet0x5 while transfering articles");
         console.log("data :",data.status, data.statusText);
         if( data.responseJSON.message==4){
           $('#alert-danger').find('label').text('Vous ne pouvez pas effectuer cette prorogation');
           $('#alert-danger').show('slow', function () {
             setTimeout(function functionName() {
               $('#alert-danger').hide('slow');
             }, 3000);
           });

         }
       }
     });
}
