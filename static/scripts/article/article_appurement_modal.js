var appurementArticleUrl =  $('#settings').attr('appurement_article_api_url');
var decdetAppurementForm = $('#article-appurement-form');
var apurementMode = 'decision';

function article_appurement_modal_init() {
  date = new Date().toISOString().substr(0, 10);
  $('#article-appurement-form-date').val(date);
  article_appurement_modal_listeners();
}


function article_appurement_modal_listeners() {

  $('#article-appurement-form-btn').on('click', function(event) {
    if (apurementMode == 'decision') {
      $('#create-appurement-form-decision').find('[type=submit]').trigger('click');
    }
    else if (apurementMode == 'decdet') {
      $('#create-appurement-form-decdet').find('[type=submit]').trigger('click');
    }
  });


  $('.apurement-form').on('submit', function(event){
    event.preventDefault();
    var articlesTableBody = $('#decdet-articles-tbody');
    var articleSelectCheckBoxs = articlesTableBody.find('input[type="checkbox"]:checked');
    var articlesPks = [];
    $.each(articleSelectCheckBoxs, function(){
        articlesPks.push($(this).attr('pk'));
    });
    var articlePksInput = $('<input>').attr({
      name : 'articlesPks',
      hidden : true
    }).val(articlesPks.toString());
    $(this).append(articlePksInput);

    var form = $(this);
    appurerArticles(form);
  });

  $('input[name="type-apurement"]').change('change', function () {
    var radioTypeApurement = $('input[name="type-apurement"]:checked');
    apurementMode = radioTypeApurement.val();
    $('#apurement-decision').slideToggle();
    $('#apurement-decdet').slideToggle();
  });



}

function appurerArticles(form) {
  var data_to_send = form.serialize()
  $('#article-appurement-modal').modal('hide');

  $.ajax({
       type       : "GET",
       url        : appurementArticleUrl,
       contentType: 'application/json',
       data       : data_to_send,
       success    : function(data){
         alert('Article(s) appuré(s) avec succès');
         location.reload();
       },
       error      : function(data){
         console.log("ERROR:DecDet0x5 while appurement articles");
         console.log("data :",data.status, data.statusText, data.responseJSON.message);
         if(data.responseJSON.message = 2){
           $('#alert-danger').find('label').text('Un des articles que vous avez sélectionnés est déja appuré');
           $('#alert-danger').show('slow', function () {
             setTimeout(function functionName() {
               $('#alert-danger').hide('slow');
             }, 3000);
           });
         }
       }
     });
}
