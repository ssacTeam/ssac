from django.urls import path, include

from .views import DecDetListView, DecDetCreateView, DecDetDetailView, DecDetUpdateView, DecDetDeleteView

app_name = 'DecDet'

urlpatterns = [
    path('list/', DecDetListView.as_view(), name = 'list'),
    path('create/', DecDetCreateView.as_view(), name = 'create'),
    path('<int:pk>', DecDetDetailView.as_view(), name = 'detail'),
    path('delete/<int:pk>', DecDetDeleteView.as_view(), name = 'delete'),
    path('update/<int:pk>', DecDetUpdateView.as_view(), name = 'update'),

    path('api/', include('DecDet.api.urls', namespace = 'api-decdet')),

]
