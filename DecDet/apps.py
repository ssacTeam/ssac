from django.apps import AppConfig


class DecdetConfig(AppConfig):
    name = 'DecDet'
    verbose_name = 'Déclaration en Détail'
