
from django.urls import path
from .views import DecDetListAPIView, DecDetCreateAPIView, DecDetDetailAPIView


app_name = 'api-decdet'


urlpatterns = [

    path('list/', DecDetListAPIView.as_view(), name = 'list'),
    path('detail/', DecDetDetailAPIView.as_view(), name = 'detail'),
    path('create/', DecDetCreateAPIView.as_view(), name = 'create')

]
