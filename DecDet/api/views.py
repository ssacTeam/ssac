from django.db.models import Q
from django.shortcuts import get_object_or_404

from rest_framework import generics, permissions
from rest_framework.views import APIView
from rest_framework.response import Response

from .serializers import DecDetListSerializer, DecDetDetailSerializer, DecDetCreateSerializer
from .pagination import DecDetListPagination
from ..models import DecDetModel

from CSV.CSV import decdetQuerysetToCSV

import datetime

class DecDetListAPIView(generics.ListAPIView):
    """docstring for DecDetListAPIView."""
    serializer_class    = DecDetListSerializer
    permission_classes  = [permissions.IsAuthenticated]
    pagination_class    = DecDetListPagination

    def get_queryset(self, *args, **kwargs):
        queryset                = DecDetModel.objects.all().order_by("-timestamp")

        query_code              = self.request.GET.get("code",None)
        query_num_decdet        = self.request.GET.get("num_decdet",None)
        query_appurement        = self.request.GET.get("appurement", None)
        query_importateur       = self.request.GET.get("importateur",None)
        query_declarant         = self.request.GET.get("declarant", None)
        query_taux_suspension   = self.request.GET.get("taux_suspension",None)
        query_numero_caution    = self.request.GET.get("numero_caution", None)
        query_numero_contrat    = self.request.GET.get("numero_contrat", None)
        query_objet_contrat     = self.request.GET.get("objet_contrat", None)
        query_lieu_execution    = self.request.GET.get("lieu_execution", None)
        query_num_autors_at     = self.request.GET.get("num_autors_at", None)
        query_year              = self.request.GET.get("year", None)
        query_month             = self.request.GET.get("month", None)

        if query_num_decdet is not None:
            queryset = queryset.filter(Q(num_decdet__icontains = query_num_decdet))
        if query_importateur is not None:
            queryset = queryset.filter(Q(importateur__icontains = query_importateur))
        if query_code is not None:
            queryset = queryset.filter(Q(code = query_code))
        if query_taux_suspension is not None:
            queryset = queryset.filter(Q(taux_suspension = query_taux_suspension))
        if query_declarant is not None:
            queryset = queryset.filter(Q(declarant__icontains = query_declarant))
        if query_objet_contrat is not None:
            queryset = queryset.filter(Q(objet_contrat__icontains = query_objet_contrat))
        if query_lieu_execution is not None:
            queryset = queryset.filter(Q(lieu_execution__icontains = query_lieu_execution))
        if query_numero_caution is not None:
            queryset = queryset.filter(Q(numero_caution__icontains = query_numero_caution))
        if query_num_autors_at is not None:
            queryset = queryset.filter(Q(num_autors_at__icontains = query_num_autors_at))
        if query_year is not None:
            queryset = queryset.filter(Q(date_autors_at__year = query_year))
        if query_month is not None:
            queryset = queryset.filter(Q(date_autors_at__month = query_month))
        last_queryset = queryset
        if query_appurement is not None:
            last_queryset = []
            for decdet in queryset:
                if decdet.getDecdetStatus() == query_appurement:
                    last_queryset.append(decdet)
        decdetQuerysetToCSV(last_queryset)
        return last_queryset

    
class DecDetDetailAPIView(APIView):
    permission_classes  = [permissions.IsAuthenticated]

    def get(self, *args, **kwargs):
        reference= self.request.GET.get("reference", None)
        decdet = get_object_or_404(DecDetModel, reference = reference)
        decdetJSON = DecDetDetailSerializer(decdet).data
        return Response(decdetJSON)

class DecDetCreateAPIView(APIView):
    serializer_class    = DecDetCreateSerializer
    permission_classes  = [permissions.IsAuthenticated]
    # renderer_classes = [renderers.JSONRenderer]

    # def perform_create(self, serializer):
    #     decdet_id = self.kwargs.get('pk')
    #     decdet = get_object_or_404(DecDetModel, pk = decdet_id)
    #     serializer.save(decdet = decdet)

    #rename to get when using the APIView
    def get(self, request, format = None):
        decdet_args = {}

        decdet_args['importateur'] = self.request.GET.get('importateur', None)
        decdet_args['code']  = self.request.GET.get('code', None)
        decdet_args['num_decdet']  = self.request.GET.get('num_decdet', None)
        decdet_args['importateur']  = self.request.GET.get('importateur', None)
        decdet_args['declarant']  = self.request.GET.get('declarant', None)
        decdet_args['val_marchandise']  = self.request.GET.get('val_marchandise', None)
        decdet_args['taux_suspension']  = self.request.GET.get('taux_suspension', None)
        decdet_args['montant_dit']  = self.request.GET.get('montant_dit', None)
        decdet_args['numero_caution']  = self.request.GET.get('numero_caution', None)
        decdet_args['date_caution']  = self.request.GET.get('date_caution', None)
        decdet_args['num_contrat']  = self.request.GET.get('num_contrat', None)
        decdet_args['objet_contrat']  = self.request.GET.get('objet_contrat', None)
        decdet_args['lieu_execution']  = self.request.GET.get('lieu_execution', None)
        decdet_args['duree_contrat']  = self.request.GET.get('duree_contrat', None)
        decdet_args['num_autors_at']  = self.request.GET.get('num_autors_at', None)
        decdet_args['date_autors_at']  = self.request.GET.get('date_autors_at', None)
        decdet_args['duree_decdet']  = self.request.GET.get('duree_decdet', None)

        decdet = DecDetModel.objects.create_decdet(decdet_args)
        if decdet :
            data = DecDetListSerializer(decdet).data
            return Response(data)
        return Response({"message" : "Impossible de créer une déclaration en détail avec ces données en entrée"}, status = 400 )


#http://localhost:8000/decdet/api/create/?code=5&date_caution=2015-08-05&declarant=fads&duree_contrat=5&duree_decdet=10&importateur=aa&lieu_execution=mll&montant_dit=5000000&numero_caution=50&objet_contrat=54djs&taux_suspension=5

#http://localhost:8000/decdet/api/create/? code=dd&importateur=dddd&declarant=d&objet_contrat=dd&lieu_execution=d&duree_contrat=0&taux_suspension=0&duree_decdet=0&montant_dit=0&numero_caution=0&date_caution=06/01/2019
