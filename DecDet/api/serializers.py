import datetime
from rest_framework import serializers
from ..models import DecDetModel
from Article.models import ArticleModel
from Article.api.serializers import ArticleSerializer



class DecDetListSerializer(serializers.ModelSerializer):
    """docstring for DecDetSerializer."""
    # decdet_update_url        = serializers.SerializerMethodField()
    # articles_api_url         = serializers.SerializerMethodField()
    decdet_detail_url        = serializers.SerializerMethodField()
    status_appurement        = serializers.SerializerMethodField()
    nb_articles_statut_retard= serializers.SerializerMethodField()



    class Meta:
        model = DecDetModel
        fields = [
            'pk',
            'code',
            'num_decdet',
            'importateur',
            'declarant',
            'val_marchandise',
            'taux_suspension',
            'montant_dit',
            'numero_caution',
            'date_caution',
            'num_contrat',
            'objet_contrat',
            'lieu_execution',
            'duree_contrat',
            'num_autors_at',
            'date_autors_at',
            'duree_decdet',

            'nb_articles_statut_retard',
            'status_appurement',
            'decdet_detail_url'
        ]

    def get_decdet_detail_url(self, object):
        return object.getDetailUrl()

    def get_nb_articles_statut_retard(self, object):
        return object.get_nb_articles_statut_retard()

    def get_status_appurement(self, object):
        return object.getDecdetStatus()


class DecDetDetailSerializer(serializers.ModelSerializer):
    """docstring for DecDetSerializer."""
    decdet_update_url        = serializers.SerializerMethodField()
    decdet_detail_url        = serializers.SerializerMethodField()
    # decdet_articles_url        = serializers.SerializerMethodField()
    # article_set              = ArticleSerializer(read_only=True, many=True)


    class Meta:
        model = DecDetModel
        fields = [
            'pk',
            'code',
            'num_decdet',
            'importateur',
            'declarant',
            'val_marchandise',
            'taux_suspension',
            'montant_dit',
            'numero_caution',
            'date_caution',
            'num_contrat',
            'objet_contrat',
            'lieu_execution',
            'duree_contrat',
            'num_autors_at',
            'date_autors_at',
            'duree_decdet',

            'decdet_update_url',
            'decdet_detail_url',
        ]

    def get_decdet_update_url(self, object):
        return object.getUpdateUrl()

    def get_decdet_detail_url(self, object):
        return object.getDetailUrl()

    def get_decdet_articles_url(self, object):
        return object.getArticleListApiUrl()

    def get_article_set(self, object):
        return ArticleModel.objects.get_article_of_decdet(object)

class DecDetCreateSerializer(DecDetListSerializer):
    """docstring for DecDetCreateSerializer."""
