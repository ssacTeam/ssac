from rest_framework import pagination

PAGE_SIZE = 20

class DecDetListPagination(pagination.PageNumberPagination):
    page_size           = PAGE_SIZE
    page_query_param    = 'num_page'
    # page_page_size      = 1000
