from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import (
                                    ListView,
                                    DetailView,
                                    CreateView,
                                    DeleteView,
                                    UpdateView
                                    )

from django.urls import reverse_lazy
from django.shortcuts import render, get_object_or_404

from .models import DecDetModel
from .forms import DecDetCreateForm

from Article.models import ArticleModel
from Prorogation.models import ProrogationModel
from Transfert.models import TransfertModel

from.api.pagination import PAGE_SIZE

class DecDetListView(LoginRequiredMixin, ListView):
    model = DecDetModel
    template_name = 'decdet_list.html'

    def get_queryset(self, *args, **kwargs):
        decdetQuerySet = DecDetModel.objects.all();
        return decdetQuerySet

    def get_context_data(self, *args, **kwargs):
        context                           = super(DecDetListView, self).get_context_data()
        context['decdet_create_url']      = DecDetModel.objects.getCreateUrl()
        context['decdet_api_list_url']    = DecDetModel.objects.getApiListUrl()
        context['PAGE_SIZE']              = PAGE_SIZE
        return context

class DecDetCreateView(LoginRequiredMixin, CreateView):
    template_name = 'decdet_create.html'
    form_class    = DecDetCreateForm
    success_url   = reverse_lazy("decdet:list")

    def get_context_data(self, *args, **kwargs):
        context                             = super(DecDetCreateView, self).get_context_data()
        context['decdet_api_create_url']    = DecDetModel.objects.getApiCreateUrl()
        context['decdet_list_url']          = DecDetModel.objects.getListUrl()
        return context

class DecDetDetailView(LoginRequiredMixin, DetailView):
    model = DecDetModel
    template_name = 'decdet_detail.html'

    def get_object(self):
        decdet_id = self.kwargs.get('pk')
        decdet    = get_object_or_404(DecDetModel, pk=decdet_id)
        return decdet

    def get_context_data(self, *args, **kwargs):
        decdet_id                       = self.kwargs.get('pk')
        context                         = super(DecDetDetailView, self).get_context_data()
        decdet                          = self.get_object()
        context['duree_restante_prorogation']  = ProrogationModel.objects.getDureeMaxProrogation(decdet)
        context['duree_restante_transfert']  = decdet.getDureeTransfertMax()
        context['date_creation']        = decdet.timestamp.strftime('%A %d %B %Y')
        context['temps_creation']       = decdet.timestamp.strftime('%H:%M:%S')
        context['decdet_update_url']    = decdet.getUpdateUrl()
        context['decdet_api_detail_url']= DecDetModel.objects.getApiDetailUrl()
        context['decdet_articles_url']  = decdet.getArticleListApiUrl()
        context['create_article_api_url']  = ArticleModel.objects.get_create_article_api_url(decdet)
        context['transfert_article_api_url']  = TransfertModel.objects.getCreateAPIUrl()
        context['prorogation_create_api_url']    = ProrogationModel.objects.getCreateAPIUrl()
        # context['price_appure']  = decdet.getPriceAppure()
        context['price_total']   = decdet.getPriceTotal()
        return context


class DecDetUpdateView(LoginRequiredMixin, UpdateView):
    form_class    = DecDetCreateForm
    template_name = 'decdet_update.html'


    def get_object(self):
        decdet_id = self.kwargs.get('pk')
        decdet    = get_object_or_404(DecDetModel, pk=decdet_id)
        return decdet

    def get_context_data(self, *args, **kwargs):
        decdet_id                     = self.kwargs.get('pk')
        context                       = super(DecDetUpdateView, self).get_context_data()
        return context

class DecDetDeleteView(LoginRequiredMixin, DeleteView):
    model = DecDetModel
    template_name = 'decdet_delete.html'
    success_url = reverse_lazy("decdet:list")
    def get_object(self):
        decdet_id = self.kwargs.get('pk')
        decdet    = get_object_or_404(DecDetModel, pk=decdet_id)
        return decdet
