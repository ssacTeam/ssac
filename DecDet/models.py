from django.db import models
from django.urls import reverse_lazy

import datetime



class DecDetManager(models.Manager):

    def getListUrl(self, *args, **kwargs):
        return reverse_lazy('decdet:list')

    def getCreateUrl(self, *args, **kwargs):
        return reverse_lazy('decdet:create')

    def getUpdateUrl(self, decdet, **kwargs):
        return decdet.getUpdateUrl()

    def getDetailUrl(self, decdet, **kwargs):
        return decdet.getUpdateUrl()

    def getApiListUrl(self, *args, **kwargs):
        return reverse_lazy('decdet:api-decdet:list')

    def getApiCreateUrl(self, *args, **kwargs):
        return reverse_lazy('decdet:api-decdet:create')

    def getApiDetailUrl(self, *args, **kwargs):
        return reverse_lazy('decdet:api-decdet:detail')

    def create_decdet(self, decdet_args, *args, **kwargs):
        try:
            new_decdet = self.model(
                code            = decdet_args['code'],
                num_decdet      = decdet_args['num_decdet'],
                importateur     = decdet_args['importateur'],
                declarant       = decdet_args['declarant'],
                val_marchandise = decdet_args['val_marchandise'],
                taux_suspension = decdet_args['taux_suspension'],
                montant_dit     = decdet_args['montant_dit'],
                numero_caution  = decdet_args['numero_caution'],
                date_caution    = decdet_args['date_caution'],
                num_contrat     = decdet_args['num_contrat'],
                objet_contrat   = decdet_args['objet_contrat'],
                lieu_execution  = decdet_args['lieu_execution'],
                duree_contrat   = decdet_args['duree_contrat'],
                num_autors_at   = decdet_args['num_autors_at'],
                date_autors_at  = decdet_args['date_autors_at'],
                duree_decdet    = decdet_args['duree_decdet']
            )
            new_decdet.save()

        except Exception as e:
            new_decdet = None
        return new_decdet


class DecDetModel(models.Model):
    """docstring for DecDetModel."""

    class Meta:
        verbose_name_plural = 'Déclarations en Détail'


    code            = models.CharField(max_length = 4, default='')
    num_decdet      = models.CharField(max_length = 255, default='', unique = True)
    importateur     = models.CharField(max_length = 255, default='')
    declarant       = models.CharField(max_length = 255, default='')
    val_marchandise = models.PositiveIntegerField(default=0)
    taux_suspension = models.PositiveIntegerField(default=0)
    montant_dit     = models.PositiveIntegerField(default=0)


    numero_caution  = models.CharField(max_length = 255, default='')
    date_caution    = models.DateField(default=datetime.date.today)

    num_contrat     = models.CharField(max_length = 255, default='')
    objet_contrat   = models.CharField(max_length = 255, default='')
    lieu_execution  = models.CharField(max_length = 255, default='')
    duree_contrat   = models.PositiveIntegerField(default=0)

    num_autors_at   = models.CharField(max_length = 255, default='')
    date_autors_at  = models.DateField(default=datetime.date.today)
    duree_decdet    = models.PositiveIntegerField(default=0)


    timestamp       = models.DateTimeField(auto_now_add=True, blank=True)# date of creation

    objects         = DecDetManager()

    def __str__(self):
        return "Admission Temporaire n°"+str(self.num_decdet)

    def get_absolute_url(self):
        return self.getDetailUrl()

    def getUpdateUrl(self, *args, **kwargs):
        return reverse_lazy('decdet:update', kwargs={"pk":self.pk})

    def getDetailUrl(self, *args, **kwargs):
        return reverse_lazy('decdet:detail', kwargs={"pk":self.pk})

    def getDeleteUrl(self, *args, **kwargs):
        return reverse_lazy('decdet:delete', kwargs={"pk":self.pk})

    def getArticleListApiUrl(self, *args, **kwargs):
        return reverse_lazy('article:api-article:decdet', kwargs={'decdet_pk' : self.pk})

    # def getProrogationApiCreateUrl(self, *args, **kwargs):
    #     return reverse_lazy('prorogation:api-prorogation:create', kwargs={'decdet_pk' : self.pk})

    def getAppurementApiCreateUrl(self, *args, **kwargs):
        return reverse_lazy('article:api-article:appurer')

    def getAppurementListUrl(self, *args, **kwargs):
        return reverse_lazy('appurement:decdet-list', kwargs={'decdet_pk' : self.pk})

    def getProrogationListUrl(self, *args, **kwargs):
        return reverse_lazy('prorogation:list', kwargs={'pk' : self.pk})

    def getTransfertsListUrl(self, *args, **kwargs):
        return reverse_lazy('transfert:decdet-list', kwargs={'pk' : self.pk})

    def getPriceAppure(self, *args, **kwargs):
        price = self.article_set.filter(appure = True).aggregate(models.Sum('valeur'))
        return price

    def getPriceTotal(self, *args, **kwargs):
        price = self.article_set.all().aggregate(models.Sum('valeur'))
        return price

    def getProrogationApiListUrl(self):
        return reverse_lazy('prorogation:api-prorogation:list', kwargs={'decdet_pk' : self.pk})


    def getDecdetStatus(self):
        status_appurement = ''
        nbPending = nbRetard = 0
        article_set = self.article_set.all()

        for article in article_set:
            article_status = article.getStatusAppurement()
            if article_status == 'retard':
                nbRetard+=1
            elif article_status == 'pending':
                nbPending+=1

        if nbRetard == len(article_set):
            status_appurement = 'retard'
        elif nbRetard !=0:
            status_appurement = 'pending-retard'
        elif nbPending !=0:
            status_appurement = 'pending'
        else:
            status_appurement = 'appure'
        return status_appurement

    def get_nb_articles_statut_retard(self):
        article_set = self.article_set.all()
        nbRetard = 0
        for article in article_set:
            article_status = article.getStatusAppurement()
            if article_status == 'retard':
                nbRetard+=1
        return nbRetard

    def getDeltaAppurement(self):
        decdet = self
        delta_appurement = (decdet.date_autors_at + datetime.timedelta(days=30)*decdet.duree_decdet) - datetime.datetime.now().date()
        return int(delta_appurement.days / 30)

    def getDureeTransfertMax(self):
        decdet = self
        delta_appurement = (decdet.date_autors_at + datetime.timedelta(days=30)*decdet.duree_decdet) - datetime.datetime.now().date()
        return int(delta_appurement.days / 30)


    @classmethod
    def getCreateUrl(self, *args, **kwargs):
        return self.objects.getCreateUrl()
    @classmethod
    def getListUrl(self, *args, **kwargs):
        return self.objects.getListUrl()
