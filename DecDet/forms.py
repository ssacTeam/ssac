import datetime
from django import forms
from ssac.forms import DateInput

from .models import DecDetModel

class DecDetCreateForm(forms.ModelForm):
    class Meta:
        model   = DecDetModel
        fields = '__all__'
        labels = {
        'num_decdet' : 'N° Déclaration',
        'declarant' : 'Déclarant',
        'val_marchandise' : 'Valeur Marchandise',
        'taux_suspension' : 'Taux de suspension',
        'montant_dit' : 'Montant D/T',
        'numero_caution' : 'N° Caution',
        'num_contrat' : 'N° Contrat',
        'lieu_execution' : 'Lieu d\'exécution',
        'duree_contrat' : 'Durée du contrat',
        'num_autors_at' : 'N° Autorisation d\'admission',
        'date_autors_at' : 'Date Autorisation d\'admission',
        'duree_decdet' : 'Durée accordée',
        }
        widgets = {
        'date_caution' : DateInput(format='%Y-%m-%d'),
        'date_autors_at' : DateInput(format='%Y-%m-%d')
        }
