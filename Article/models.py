from django.db import models
from django.urls import reverse_lazy

from DecDet.models import DecDetModel
from Prorogation.models import ProrogationModel
import datetime

class ArticleManager(models.Manager):
    """docstring for ArticleManager."""
    def get_article_of_decdet(self, decdet):
        article_queryset = ArticleModel.objects.filter(decdet = decdet)
        return article_queryset

    def get_article_of_prorogation(self, prorogation):
        return prorogation.article_set.all()

    def get_create_article_api_url(self, decdet):
        return reverse_lazy('article:api-article:create', kwargs={'decdet_pk' : decdet.pk})

    def get_transfert_article_api_url(self, decdet):
        return reverse_lazy('article:api-article:transfert')

    def transfert_articles_of_decdet(self, articlesPks, curDecdetPk, newDecdetpk):
        if curDecdetPk and newDecdetpk and articlesPks:
            for articlePk in articlesPks:
                ArticleModel.objects.filter(pk = articlePk).update(decdet = newDecdetpk)
            return True

        return False

    def appurer_articles(self, articlesPks):
        if articlesPks is not None:
            for articlePk in articlesPks:
                ArticleModel.objects.filter(pk = articlePk).update(appure = True)
            return True
        return False

    def proroger_articles(self, articlesPks, prorogation):
        if articlesPks is not None:
            for articlePk in articlesPks:
                article = ArticleModel.objects.get(pk = articlePk)
                if article.prorogation:
                    article.prev_prorogation.add(article.prorogation)
                article.prorogation = prorogation
                article.save()
            return True
        return False

    def create_article_of_decdet(self, decdet, article_args):
        try:
            new_article = self.model(
                            decdet              = decdet,
                            num_serie           = article_args['num_serie'],
                            position_tarifaire  = article_args['position_tarifaire'],
                            valeur              = article_args['valeur'],
                            quantite            = article_args['quantite'],
                            description         = article_args['description'],
                            )
            new_article.save()
        except Exception as e:
            print(e)
            new_article = None
        return new_article


class ArticleModel(models.Model):
    """docstring for ArticleModel."""

    class Meta:
        verbose_name_plural = 'Articles'

    decdet              = models.ForeignKey(DecDetModel, default=1, on_delete=models.CASCADE, related_name='article_set')
    prorogation         = models.ForeignKey(ProrogationModel, on_delete=models.SET_NULL, related_name='article_set', blank=True, null=True)
    prev_prorogation    = models.ManyToManyField(ProrogationModel, related_name='prev_article_set', blank=True)
    num_serie           = models.CharField(default='', max_length = 255)
    position_tarifaire  = models.CharField(default='', max_length = 255)
    valeur              = models.PositiveIntegerField(default=0)
    quantite            = models.PositiveIntegerField(default=0)
    description         = models.CharField(max_length = 255, default='')
    appure              = models.BooleanField(null=False, default=False)

    objects = ArticleManager()

    def __str__(self):
        return "Article n°"+str(self.num_serie)

    def getUpdateUrl(self):
        return reverse_lazy('article:update', kwargs={'pk' : self.pk})

    def getAppure(self):
        if self.appure:
            return 'Appuré'
        else:
            return 'NON-Appuré'

    def apurer(self):
        self.appure = True
        self.save()

    def getStatusAppurement(self):
        dec = self.decdet
        if self.prorogation:
            dec = self.prorogation
        delta_appurement = dec.getDeltaAppurement()
        status = str()
        if self.appure:
            status = 'appure'
        elif delta_appurement > 0 :
            status = 'pending'
        else:
            status = 'retard'
        return status
