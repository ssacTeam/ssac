from django.urls import path, include

from .views import ArticleCreateView, ArticleUpdateView

app_name = 'Article'

urlpatterns = [
    path('create/<int:decdet_pk>', ArticleCreateView.as_view(), name = 'create'),
    path('update/<int:pk>', ArticleUpdateView.as_view(), name = 'update'),
    path('api/', include('Article.api.urls', namespace = 'api-article'))
]
