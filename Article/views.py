from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import (
                                    ListView,
                                    DetailView,
                                    CreateView,
                                    DeleteView,
                                    UpdateView
                                    )
from django.shortcuts import render, get_object_or_404

from django.urls import reverse_lazy

from DecDet.models import DecDetModel

from .models import ArticleModel

from .forms import ArticleCreateForm, ArticleUpdateForm

class ArticleCreateView(LoginRequiredMixin, CreateView):
    template_name = 'article_create.html'
    form_class    = ArticleCreateForm
    success_url   = DecDetModel.objects.getListUrl()

    def get_context_data(self, *args, **kwargs):
        context                             = super(ArticleCreateView, self).get_context_data()
        context['decdet_pk']                = self.kwargs.get('decdet_pk')
        return context


class ArticleUpdateView(LoginRequiredMixin, UpdateView):
    form_class    = ArticleUpdateForm
    template_name = 'article_update.html'

    def get_success_url(self):
        article_pk = self.kwargs.get('pk')
        article    = get_object_or_404(ArticleModel, pk=article_pk)
        decdet     = article.decdet
        return decdet.getDetailUrl()

    def get_object(self):
        article_id = self.kwargs.get('pk')
        article    = get_object_or_404(ArticleModel, pk=article_id)
        return article

    def get_context_data(self, *args, **kwargs):
        decdet_id                     = self.kwargs.get('pk')
        context                       = super(ArticleUpdateView, self).get_context_data()
        return context
