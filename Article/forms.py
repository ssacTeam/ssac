from django import forms

from .models import ArticleModel


class ArticleCreateForm(forms.ModelForm):
    """docstring for ArticleCreateForm."""

    class Meta:
        model   = ArticleModel
        fields = '__all__'




class ArticleUpdateForm(forms.ModelForm):
    """docstring for ArticleCreateForm."""

    class Meta:
        model   = ArticleModel
        fields = [
            'num_serie',
            'position_tarifaire',
            'valeur',
            'quantite',
            'description',
        ]
