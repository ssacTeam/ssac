from rest_framework import serializers
import datetime
from ..models import ArticleModel

class ArticleSerializer(serializers.ModelSerializer):
    """docstring for DecDetSerializer."""

    article_update_url      = serializers.SerializerMethodField()
    status_appurement       = serializers.SerializerMethodField()
    delta_appurement        = serializers.SerializerMethodField()
    transfertBool           = serializers.SerializerMethodField()
    prorogationBool         = serializers.SerializerMethodField()
    lastTransfertUrl        = serializers.SerializerMethodField()
    prorogationUrl          = serializers.SerializerMethodField()
    prorogationNum          = serializers.SerializerMethodField()

    class Meta:
        model = ArticleModel
        fields = [
            'pk',
            'num_serie',
            'position_tarifaire',
            'valeur',
            'quantite',
            'description',
            'prorogation',

            'status_appurement',
            'delta_appurement',
            'prorogationNum',

            'transfertBool',
            'prorogationBool',

            'lastTransfertUrl',
            'article_update_url',
            'prorogationUrl'

        ]

    def get_transfertBool(self, object):
        if not object.transfert_set.all() :
            return False
        return True

    def get_prorogationBool(self, object):
        if not object.prorogation :
            return False
        return True

    def get_lastTransfertUrl(self, object):
        transfert = object.transfert_set.all().order_by('-timestamp').first()
        if transfert is not None:
            return transfert.getDetailUrl()
        return None

    def get_prorogationUrl(self, object):
        if self.get_prorogationBool(object):
            return object.prorogation.getDetailUrl()
        return None

    def get_prorogationNum(self, object):
        if self.get_prorogationBool(object):
            return object.prorogation.num_decdet
        return None

    def get_article_update_url(self, object, *args, **kwargs):
        return object.getUpdateUrl()

    def get_status_appurement(self, object, *args, **kwargs):
        return object.getStatusAppurement()

    def get_delta_appurement(self, object, *args, **kwargs):
        dec = object.decdet
        if object.prorogation:
            dec = object.prorogation
        return dec.getDeltaAppurement()
