from django.db.models import Q
from django.shortcuts import get_object_or_404

from rest_framework import generics, permissions, renderers
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated

from .serializers import ArticleSerializer
from ..models import ArticleModel
from DecDet.models import DecDetModel
from Transfert.models import TransfertModel
from Appurement.models import AppurementModel
from Prorogation.models import ProrogationModel

from CSV.CSV import articleQuerysetToCSV

class ArticlesListAPIView(generics.ListAPIView):
    """docstring for DecDetListAPIView."""
    serializer_class    = ArticleSerializer
    permission_classes  = [permissions.IsAuthenticated]
    # permission_classes  = [permissions.IsAuthenticated]

    def get_queryset(self, *args, **kwargs):
        queryset        = ArticleModel.objects.all()
        return queryset

class ArticleDecDetListAPIView(generics.ListAPIView):
    """docstring for DecDetListAPIView."""
    serializer_class    = ArticleSerializer
    permission_classes  = [permissions.IsAuthenticated]
    # permission_classes  = [permissions.IsAuthenticated]

    def get_queryset(self, *args, **kwargs):
        decdet_pk       = self.kwargs.get('decdet_pk')
        decdet          = get_object_or_404(DecDetModel, pk = decdet_pk)
        queryset        = ArticleModel.objects.get_article_of_decdet(decdet)
        articleQuerysetToCSV(queryset)
        return queryset

class ArticleProrogationListAPIView(generics.ListAPIView):
    """docstring for DecDetListAPIView."""
    serializer_class    = ArticleSerializer
    permission_classes  = [permissions.IsAuthenticated]
    # permission_classes  = [permissions.IsAuthenticated]

    def get_queryset(self, *args, **kwargs):
        pk            = self.kwargs.get('prorogation_pk')
        prorogation   = get_object_or_404(ProrogationModel, pk = pk)
        queryset       = ArticleModel.objects.get_article_of_prorogation(prorogation)
        articleQuerysetToCSV(queryset)
        return queryset

class ArticleCreateAPIView(APIView):
    serializer_class    = ArticleSerializer
    permission_classes  = [permissions.IsAuthenticated]
    # renderer_classes = [renderers.JSONRenderer]

    # def perform_create(self, serializer):
    #     decdet_id = self.kwargs.get('pk')
    #     decdet = get_object_or_404(DecDetModel, pk = decdet_id)
    #     serializer.save(decdet = decdet)

        #rename to get when using the APIView
    def get(self, request, decdet_pk, format = None):
        decdet = get_object_or_404(DecDetModel, pk = decdet_pk)
        article_args = {}
        article_args['num_serie'] = self.request.GET.get("num_serie",None)
        article_args['position_tarifaire'] = self.request.GET.get("position_tarifaire",None)
        article_args['valeur'] = self.request.GET.get("valeur",None)
        article_args['quantite'] = self.request.GET.get("quantite",None)
        article_args['description'] = self.request.GET.get("description",None)

        article = ArticleModel.objects.create_article_of_decdet( decdet, article_args)
        if article :
            data = ArticleSerializer(article).data
            return Response(data)
        return Response({"message" : "Impossible de créer un article avec ces données en entrée"}, status = 400 )

#http://localhost:8000/article/api/create/1?num_serie=542&position_tarifaire=458&valeur=5498&quantite=5&description=jnpzqfg

class ArticlesTransfertAPIView(APIView):
    """docstring for ArticleTransfertAPIView."""
    def get(self, request, format = None):
        article_args = {}
        prorogation_args['decdet'] = self.request.GET.get('decdet', None)

        if articlePks:
            articlePks = articlePks.split(',')
            transfert = TransfertModel.objects.create_transfert( articlePks, curDecdetPk, newDecdetpk, dateTransfert)
            if transfert:
                response  = ArticleModel.objects.transfert_articles_of_decdet( articlePks, curDecdetPk, newDecdetpk)
                if response :
                    return Response({"message" : "Transfert effectué avec succès"}, status = 200)
            return Response({"message" : "4"}, status = 400 )

        return Response({"message" : "Impossible de transférer d'articles avec ces données en entrée"}, status = 400 )


class ArticlesAppurerAPIView(APIView):
    """docstring for ArticleAppurerAPIView."""
    def get(self, request, format = None):
        apurement_args = {}
        apurement_args['articlesPks'] = self.request.GET.get('articlesPks', None)
        apurement_args['decdet'] = self.request.GET.get('decdet', None)
        apurement_args['date_appurement'] = self.request.GET.get('date_appurement', None)
        apurement_args['objet_appurement'] = self.request.GET.get('objet_appurement', None)
        apurement_args['num_decision'] = self.request.GET.get('num_decision', None)
        apurement_args['code'] = self.request.GET.get('code', None)
        apurement_args['num_decdet'] = self.request.GET.get('num_decdet', None)
        apurement_args['importateur'] = self.request.GET.get('importateur', None)
        apurement_args['declarant'] = self.request.GET.get('declarant', None)
        apurement_args['val_marchandise'] = self.request.GET.get('val_marchandise', None)
        apurement_args['taux_suspension'] = self.request.GET.get('taux_suspension', None)
        apurement_args['montant_dit'] = self.request.GET.get('montant_dit', None)
        apurement_args['num_appurement'] = self.request.GET.get('num_appurement', None)
        articlePks = apurement_args['articlesPks']
        if articlePks:
            articlePks = articlePks.split(',')
            apurement = AppurementModel.objects.create_apurement(apurement_args)
            if apurement is not None:
                return Response({"message" : "Transfert effectué avec succès"}, status = 200)
            return Response({"message" : "4"}, status = 400 )
        return Response({"message" : "Impossible de transférer d'articles avec ces données en entrée"}, status = 400 )
