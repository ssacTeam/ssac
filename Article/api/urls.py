from django.urls import path, include

from .views import (    ArticleDecDetListAPIView,
                        ArticleCreateAPIView,
                        ArticlesTransfertAPIView,
                        ArticlesListAPIView,
                        ArticlesAppurerAPIView,
                        ArticleProrogationListAPIView
                     )

app_name = 'api-article'

urlpatterns = [
    path('list/decdet/<int:decdet_pk>/', ArticleDecDetListAPIView.as_view(), name = 'decdet'),
    path('list/prorogation/<int:prorogation_pk>/', ArticleProrogationListAPIView.as_view(), name = 'prorogation'),
    path('create/<int:decdet_pk>/', ArticleCreateAPIView.as_view(), name = 'create'),
    path('transfert/', ArticlesTransfertAPIView.as_view(), name = 'transfert'),
    path('appurer/', ArticlesAppurerAPIView.as_view(), name = 'appurer'),
    path('list/', ArticlesListAPIView.as_view(), name = 'list'),

]
