"""ssac URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib.auth import views as auth_views
from django.contrib import admin
from django.urls import path, include
from .views import DashBoardView, SSACView

urlpatterns = [
    path('', SSACView.as_view(), name = 'Bienvenue'),
    path('accueil/', DashBoardView.as_view(), name = 'home'),
    path('decdet/', include('DecDet.urls', namespace = 'decdet')),
    path('article/', include('Article.urls', namespace = 'article')),
    path('transfert/', include('Transfert.urls', namespace='transfert')),
    path('prorogation/', include('Prorogation.urls', namespace='prorogation')),
    path('appurement/', include('Appurement.urls', namespace='appurement')),


    path('admin/', admin.site.urls, name = 'admin'),
    path('accounts/login/', auth_views.LoginView.as_view(template_name='login.html')),
    path('accounts/', include('django.contrib.auth.urls'))

]
