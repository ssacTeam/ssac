PRODUCTION_MODE = False

if PRODUCTION_MODE:
    from .production import *
else:
    from .local import *
