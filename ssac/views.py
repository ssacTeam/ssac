from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.views.generic.base import TemplateView
from django.urls import reverse_lazy
import datetime
from DecDet.models import DecDetModel
from Transfert.models import TransfertModel
from Prorogation.models import ProrogationModel


class DashBoardView(LoginRequiredMixin, TemplateView):

    def __init__(self, *args, **kwargs):
        super(DashBoardView, self).__init__()
        self.decdetRetard = []
        self.decdetAppure = []
        self.decdetPendin = []
        self.decdetPendinRetard = []

        self.at_apure = {
            2018 : 0,
            2019 : 0,
            2020 : 0,
            2021 : 0,
        }

    template_name = "home.html"
    login_url          = "login"

    def get_context_data(self, *args, **kwargs):
        queryset = DecDetModel.objects.all()
        for decdet in queryset:
            status = decdet.getDecdetStatus()
            if(status == 'retard'):
                self.decdetRetard.append(decdet)
            elif(status == 'pending-retard'):
                self.decdetPendinRetard.append(decdet)
            elif(status == 'pending'):
                self.decdetPendin.append(decdet)
            else:
                self.decdetAppure.append(decdet)
        for year in range(2018, 2022):
            qs = queryset.filter(Q(date_autors_at__year = year))
            for decdet in qs:
                status = decdet.getDecdetStatus()
                if status == 'appure':
                    self.at_apure[year] += 1

        context = {
            'retard' : len(self.decdetRetard),
            'appure' : len(self.decdetAppure),
            'pending': len(self.decdetPendin),
            'at_year': {
                'at_2018' : len(queryset.filter(Q(date_autors_at__year = 2018))),
                'at_2019' : len(queryset.filter(Q(date_autors_at__year = 2019))),
                'at_2020' : len(queryset.filter(Q(date_autors_at__year = 2020))),
                'at_2021' : len(queryset.filter(Q(date_autors_at__year = 2021)))
            },
            'at_apure': {
                'at_2018' : self.at_apure[2018],
                'at_2019' : self.at_apure[2019],
                'at_2020' : self.at_apure[2020],
                'at_2021' : self.at_apure[2021]
            },
            'pending_retard': len(self.decdetPendinRetard),
            'total'  : len(self.decdetRetard) + len(self.decdetAppure) + len(self.decdetPendin),
            'total_transferts' : len(TransfertModel.objects.all()),
            'total_prorogation': len(ProrogationModel.objects.all())
        }
        return context

class SSACView(TemplateView):
    template_name = "SSAC.html"
    login_url     = "login"
